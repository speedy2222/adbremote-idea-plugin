package com.remoterapp.adb.ui;

import com.intellij.openapi.ui.DialogWrapper;
import com.remoterapp.adb.util.Constants;
import com.remoterapp.adb.util.Util;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * TODO Add description for class
 *
 * @author Rob Slama
 * @version 1
 */
public class AndroidHomeSetDialog extends DialogWrapper {

    private SavePropertyPath parent;

    public AndroidHomeSetDialog(SavePropertyPath parent) {
        super(Constants.PROJECT, false);
        this.parent = parent;

        setTitle("ADB path configuration");
        init();
        show();
    }

    @Nullable
    @Override
    protected JComponent createCenterPanel() {
        return createContentPanel();
    }

    private JPanel createContentPanel() {
        final JPanel panel = new JPanel();
        label = new javax.swing.JLabel();
        jTextField1 = new JTextField();
        openFileChooserButton = new javax.swing.JButton();

        label.setText("Select ADB file or file absolute path to ADB");


        Constants.PREFERENCE_BEAN.loadState();

        if (!Util.isEmpty(Constants.PREFERENCE_BEAN.getAdbPath())) {
            jTextField1.setText(Constants.PREFERENCE_BEAN.getAdbPath());
        }


        openFileChooserButton.setText("...");
        openFileChooserButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                performOpenFileChooser(panel);
            }
        });


        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(panel);
        panel.setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addGap(19, 19, 19)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addGroup(layout.createSequentialGroup()
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                        .addComponent(label, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                        .addComponent(jTextField1))
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(openFileChooserButton, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addContainerGap(58, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addGap(15, 15, 15)
                                .addComponent(label)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(openFileChooserButton))
                                .addGap(20, 20, 20)
                                .addContainerGap(43, Short.MAX_VALUE))

        );
        return panel;
    }

    private void performOpenFileChooser(JPanel panel) {
        final FileDialog fc = new FileDialog(new JDialog(), "Select file", FileDialog.LOAD);
        fc.setModal(true);
        fc.setVisible(true);
        /*fc.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {

                if (event.getActionCommand().equals("ApproveSelection")) {
                    File selectedFile = fc.getSelectedFile();

                }
            }
        });
        int status = fc.showOpenDialog(panel);
        if (status == JFileChooser.APPROVE_OPTION) {*/
        //File selectedFile = ;
        jTextField1.setText(fc.getDirectory() + fc.getFile());
        /*} else if (status == JFileChooser.CANCEL_OPTION) {


        } */
    }


    private javax.swing.JLabel label;
    JTextField jTextField1;
    private javax.swing.JButton openFileChooserButton;


    @Override
    protected void doOKAction() {
        if (jTextField1.getText() != null && jTextField1.getText().length() > 0) {
            parent.saveAndClose(jTextField1.getText());
            AndroidHomeSetDialog.this.dispose();
        }
    }
}

