package com.remoterapp.adb.ui;

import com.intellij.openapi.wm.ToolWindowFactory;

import com.remoterapp.adb.PluginStartPoint;
import com.remoterapp.adb.util.Constants;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * TODO Add description for class
 *
 * @author Rob Slama
 * @version 1
 */
public class ADBNotConfiguredPanel extends JPanel implements SavePropertyPath{

    private ToolWindowFactory startPoint;

    public ADBNotConfiguredPanel(ToolWindowFactory startPoint){
        initComponents();
        this.startPoint = startPoint;
        setVisible(true);
    }

    private void initComponents() {

        alertLabel = new javax.swing.JLabel();
        openConfigurationButton = new javax.swing.JButton();

        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        alertLabel.setText("<html><b><font color=red>ADB is not configured.</font></b><br> Please press button to set correct path</html>");
        add(alertLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(15, 6, 244, 40));

        openConfigurationButton.setText("open configuration");
        openConfigurationButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                performOpenAction(event);
            }
        });
        add(openConfigurationButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 50, -1, -1));
    }

    private void performOpenAction(ActionEvent event){
        new AndroidHomeSetDialog(this);
    }

    public void saveAndClose(String adbPath){

        Constants.PREFERENCE_BEAN.setAdbPath(adbPath);
        Constants.PREFERENCE_BEAN.save();
        if(startPoint instanceof PluginStartPoint){
             PluginStartPoint.reloadPluginUI();
        }


    }

    // Variables declaration - do not modify
    private javax.swing.JButton openConfigurationButton;
    private javax.swing.JLabel alertLabel;
}
