package com.remoterapp.adb.ui.panels;

import com.android.ddmlib.AdbCommandRejectedException;
import com.android.ddmlib.IDevice;
import com.android.ddmlib.ShellCommandUnresponsiveException;
import com.android.ddmlib.TimeoutException;
import com.intellij.openapi.util.IconLoader;

import com.remoterapp.adb.components.JXButton;
import com.remoterapp.adb.util.Constants;
import com.remoterapp.adb.util.ShellOutputReceiverImpl;
import com.remoterapp.adb.util.Util;
import layout.TableLayout;


import javax.swing.*;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.Map;

/**
 * TODO Add description for class
 *
 * @author Rob Slama
 * @version 1
 */
public class VirtualKeysPanelUI extends JPanel{

    public VirtualKeysPanelUI(){
        init();
    }
    private void init(){
        JButton backButton;
        JButton deleteButton;
        JButton homeButton;
        JButton switchAppButton;
        JPanel buttonsPanel = new JPanel();


        setMinimumSize(new Dimension(200, 200));
        setMaximumSize(new Dimension(500, 200));
        setPreferredSize(new Dimension(300, 200));

        setLayout(new BorderLayout());

        Icon icon = IconLoader.getIcon(Constants.IMAGE_FOLDER_PATH + "ic_back.png");
        Icon icon_pressed = IconLoader.getIcon(Constants.IMAGE_FOLDER_PATH + "ic_back_pressed.png");
        Icon icon_over = IconLoader.getIcon(Constants.IMAGE_FOLDER_PATH + "ic_back_over.png");
        backButton = new JXButton(icon, icon_pressed, icon_over);


        icon = IconLoader.getIcon(Constants.IMAGE_FOLDER_PATH + "ic_home.png");
        icon_pressed = IconLoader.getIcon(Constants.IMAGE_FOLDER_PATH + "ic_home_pressed.png");
        icon_over = IconLoader.getIcon(Constants.IMAGE_FOLDER_PATH + "ic_home_over.png");
        homeButton = new JXButton(icon, icon_pressed, icon_over);

        icon = IconLoader.getIcon(Constants.IMAGE_FOLDER_PATH + "ic_delete.png");
        icon_pressed = IconLoader.getIcon(Constants.IMAGE_FOLDER_PATH + "ic_delete_pressed.png");
        icon_over = IconLoader.getIcon(Constants.IMAGE_FOLDER_PATH + "ic_delete_over.png");
        deleteButton = new JXButton(icon, icon_pressed, icon_over);

        icon = IconLoader.getIcon(Constants.IMAGE_FOLDER_PATH + "ic_switch_app.png");
        icon_pressed = IconLoader.getIcon(Constants.IMAGE_FOLDER_PATH + "ic_switch_app_pressed.png");
        icon_over = IconLoader.getIcon(Constants.IMAGE_FOLDER_PATH + "ic_switch_app_over.png");
        switchAppButton = new JXButton(icon, icon_pressed, icon_over);

        buttonsPanel.setBorder(
                new EmptyBorder(0, 10, 10, 10));
        buttonsPanel.setMinimumSize(new Dimension(200, 200));
        buttonsPanel.setMaximumSize(new Dimension(500, 200));
        buttonsPanel.setPreferredSize(new Dimension(300, 200));

        //backButton.setText("BACK");
        backButton.addActionListener(new ButtonActionListener(ButtonActionListener.ACTION_COMMAND_BACK));

        //homeButton.setText("HOME");
        homeButton.addActionListener(new ButtonActionListener(ButtonActionListener.ACTION_COMMAND_HOME));

        //deleteButton.setText("DELETE");
        deleteButton.addActionListener(new ButtonActionListener(ButtonActionListener.ACTION_COMMAND_DELETE));

        //switchAppButton.setText("SWITCH");
        switchAppButton.addActionListener(new ButtonActionListener(ButtonActionListener.ACTION_COMMAND_APP_SWITCH));

        //set panel title
        JLabel titleLabel = new JLabel();
        titleLabel.setFont(Constants.BOLD_12_FONT); // NOI18N
        titleLabel.setText("Virtual Keys:");
        titleLabel.setBorder(new EmptyBorder(10, 10, 5, 10));


        Box keysBox = Box.createHorizontalBox();

        keysBox.add(backButton);

        keysBox.add(Box.createHorizontalStrut(40));
        keysBox.add(homeButton);

        keysBox.add(Box.createHorizontalStrut(40));
        keysBox.add(switchAppButton);

        keysBox.add(Box.createHorizontalStrut(40));
        keysBox.add(deleteButton);




        icon = IconLoader.getIcon(Constants.IMAGE_FOLDER_PATH + "ic_arrow_up.png");
        icon_pressed = IconLoader.getIcon(Constants.IMAGE_FOLDER_PATH + "ic_arrow_up_pressed.png");
        icon_over = IconLoader.getIcon(Constants.IMAGE_FOLDER_PATH + "ic_arrow_up_over.png");
        JXButton arrowUpButton = new JXButton(icon, icon_pressed, icon_over);


        icon = IconLoader.getIcon(Constants.IMAGE_FOLDER_PATH + "ic_arrow_down.png");
        icon_pressed = IconLoader.getIcon(Constants.IMAGE_FOLDER_PATH + "ic_arrow_down_pressed.png");
        icon_over = IconLoader.getIcon(Constants.IMAGE_FOLDER_PATH + "ic_arrow_down_over.png");
        JXButton arrowDownButton = new JXButton(icon, icon_pressed, icon_over);


        icon = IconLoader.getIcon(Constants.IMAGE_FOLDER_PATH + "ic_arrow_left.png");
        icon_pressed = IconLoader.getIcon(Constants.IMAGE_FOLDER_PATH + "ic_arrow_left_pressed.png");
        icon_over = IconLoader.getIcon(Constants.IMAGE_FOLDER_PATH + "ic_arrow_left_over.png");
        JXButton arrowLeftButton = new JXButton(icon, icon_pressed, icon_over);


        icon = IconLoader.getIcon(Constants.IMAGE_FOLDER_PATH + "ic_arrow_right.png");
        icon_pressed = IconLoader.getIcon(Constants.IMAGE_FOLDER_PATH + "ic_arrow_right_pressed.png");
        icon_over = IconLoader.getIcon(Constants.IMAGE_FOLDER_PATH + "ic_arrow_right_over.png");
        JXButton arrowRightButton = new JXButton(icon, icon_pressed, icon_over);


        icon = IconLoader.getIcon(Constants.IMAGE_FOLDER_PATH + "ic_dpad_ok.png");
        icon_pressed = IconLoader.getIcon(Constants.IMAGE_FOLDER_PATH + "ic_dpad_ok_pressed.png");
        icon_over = IconLoader.getIcon(Constants.IMAGE_FOLDER_PATH + "ic_dpad_ok_over.png");
        JXButton arrowCenterButton = new JXButton(icon, icon_pressed, icon_over);


        arrowCenterButton.addActionListener(new ButtonActionListener(ButtonActionListener.ACTION_COMMAND_DPAP_CENTER));
        arrowUpButton.addActionListener(new ButtonActionListener(ButtonActionListener.ACTION_COMMAND_DPAP_UP));
        arrowDownButton.addActionListener(new ButtonActionListener(ButtonActionListener.ACTION_COMMAND_DPAP_DOWN));
        arrowLeftButton.addActionListener(new ButtonActionListener(ButtonActionListener.ACTION_COMMAND_DPAP_LEFT));
        arrowRightButton.addActionListener(new ButtonActionListener(ButtonActionListener.ACTION_COMMAND_DPAP_RIGHT));

        double sizes[][] = {
                {0.30,0.40,0.30},
                {0.30,0.40,0.30}};
        TableLayout tableLayout = new TableLayout(sizes);
        JPanel dpad = new JPanel(tableLayout);
        dpad.setMaximumSize(new Dimension(120,120));
        dpad.add(arrowUpButton, "1,0,t");
        dpad.add(arrowLeftButton, "0,1,r");
        dpad.add(arrowCenterButton, "1,1");
        dpad.add(arrowRightButton, "2,1,l");
        dpad.add(arrowDownButton, "1,2");

        Box verticalBox = Box.createVerticalBox();
        verticalBox.add(keysBox);
        verticalBox.add(Box.createRigidArea(new Dimension(0,30)));
        verticalBox.add(dpad);

        buttonsPanel.add(verticalBox, BorderLayout.CENTER);
        add(titleLabel, BorderLayout.PAGE_START);
        add(buttonsPanel, BorderLayout.CENTER);

    }



    private class ButtonActionListener implements ActionListener {

        private static final String ACTION_COMMAND_HOME = "input keyevent 3";
        private static final String ACTION_COMMAND_BACK = "input keyevent 4";
        private static final String ACTION_COMMAND_APP_SWITCH = "input keyevent 187";
        private static final String ACTION_COMMAND_DELETE = "input keyevent 67";

        private static final String ACTION_COMMAND_DPAP_CENTER = "input keyevent 23";
        private static final String ACTION_COMMAND_DPAP_UP = "input keyevent 19";
        private static final String ACTION_COMMAND_DPAP_DOWN = "input keyevent 20";
        private static final String ACTION_COMMAND_DPAP_LEFT = "input keyevent 21";
        private static final String ACTION_COMMAND_DPAP_RIGHT = "input keyevent 22";


        private final String actionCommand;

        public ButtonActionListener(String actionCommand) {
            this.actionCommand = actionCommand;
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {

            for (Map.Entry<String, Boolean> entry : Constants.DEVICE_STATE.entrySet()) {
                if (entry.getValue()) {
                    final IDevice iDevice = Util.findDeviceBySerial(Constants.CONNECTED_DEVICES, entry.getKey());
                    if (iDevice != null && iDevice.isOnline()) {

                        new Thread() {
                            @Override
                            public void run() {
                                super.run();
                                try {
                                    iDevice.executeShellCommand(actionCommand, new ShellOutputReceiverImpl(iDevice, ""));
                                } catch (TimeoutException e1) {
                                    e1.printStackTrace();
                                } catch (AdbCommandRejectedException e1) {
                                    e1.printStackTrace();
                                } catch (ShellCommandUnresponsiveException e1) {
                                    e1.printStackTrace();
                                } catch (IOException e1) {
                                    e1.printStackTrace();
                                }
                            }
                        }.start();

                    }
                }
            }

        }
    }

}
