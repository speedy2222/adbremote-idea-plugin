package com.remoterapp.adb.ui.panels;

import com.remoterapp.adb.PluginStartPoint;
import com.remoterapp.adb.ui.AndroidHomeSetDialog;
import com.remoterapp.adb.ui.SavePropertyPath;
import com.remoterapp.adb.util.Constants;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * TODO Add description for class
 *
 * @author Rob Slama
 * @version 1
 */
public class SettingsPanelUI extends JPanel implements SavePropertyPath {

    public SettingsPanelUI() {
        init();
    }

    public void init() {
        JButton settingsButton;

        JLabel settingsLabel = new javax.swing.JLabel();


        setMinimumSize(new Dimension(200, 80));
        setMaximumSize(new Dimension(400, 80));
        setPreferredSize(new Dimension(300, 80));
        setLayout(new BorderLayout());

        settingsButton = new JButton("ADB Path Settings");


        settingsButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                 new AndroidHomeSetDialog(SettingsPanelUI.this);
            }
        });
        settingsButton.setAlignmentX(Component.LEFT_ALIGNMENT);
        settingsButton.setAlignmentY(Component.TOP_ALIGNMENT);

        settingsLabel.setFont(Constants.BOLD_12_FONT);
        settingsLabel.setText("Settings:");
        settingsLabel.setAlignmentX(Component.LEFT_ALIGNMENT);
        settingsLabel.setAlignmentY(Component.TOP_ALIGNMENT);
        settingsLabel.setBorder(new EmptyBorder(10, 10, 10, 10));


        Box box = Box.createVerticalBox();
        box.add(settingsButton);
        box.setBorder(new EmptyBorder(0,0,10,10));





        add(settingsLabel, BorderLayout.PAGE_START);
        add(box, BorderLayout.CENTER);
    }

    @Override
    public void saveAndClose(String adbPath) {
        Constants.PREFERENCE_BEAN.setAdbPath(adbPath);
        Constants.PREFERENCE_BEAN.save();
        PluginStartPoint.reloadPluginUI();
    }
}
