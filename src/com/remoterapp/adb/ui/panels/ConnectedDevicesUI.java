package com.remoterapp.adb.ui.panels;

import com.android.ddmlib.*;
import com.intellij.openapi.util.IconLoader;
import com.intellij.ui.components.JBScrollPane;
import com.intellij.util.ui.UIUtil;
import com.remoterapp.adb.components.CustomJCheckBox;
import com.remoterapp.adb.components.JXButton;

import com.remoterapp.adb.components.JXLabel;
import com.remoterapp.adb.exception.NotSetEnvironment;
import com.remoterapp.adb.ui.dialogs.DeviceInfoDialog;
import com.remoterapp.adb.util.*;
import groovyjarjarantlr.Utils;


import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

/**
 * TODO Add description for class
 *
 * @author Rob Slama
 * @version 1
 */
public class ConnectedDevicesUI extends JPanel implements AdbConnector.CallBack, ItemListener, BatteryLevelThread.BatteryLevelUpdater {

    private JPanel deviceListPanel;
    private final ArrayList<JXLabel<IDevice>> batteryLabels = new ArrayList<JXLabel<IDevice>>();

    public ConnectedDevicesUI() {
        init();

        setVisible(true);
        try {
            AdbConnector.getInstance().addCallBackListener(this);
        } catch (NotSetEnvironment notSetEnvironment) {
            notSetEnvironment.printStackTrace();
        }
        deviceList();
        BatteryLevelThread.getInstance().addListener(this);
    }


    private void init() {
        //set panel's params
        Util.setSizes(this);
        setLayout(new BorderLayout());

        //set panel title
        JLabel titleLabel = new JLabel();
        titleLabel.setFont(Constants.BOLD_12_FONT); // NOI18N
        titleLabel.setText("Connected device:");
        titleLabel.setBorder(new EmptyBorder(10, 10, 5, 10));

        //devicelist panel
        this.deviceListPanel = new JPanel();

        //scrollpanel with devices list
        JBScrollPane jScrollPane = new JBScrollPane();
        jScrollPane.setBackground(UIUtil.getPanelBackground());

        jScrollPane.setBorder(new CompoundBorder(
                new EmptyBorder(0, 10, 10, 10),
                new LineBorder(Color.GRAY, 1)));


        jScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        jScrollPane.setViewportView(this.deviceListPanel);

        //add components into main panel
        add(titleLabel, BorderLayout.PAGE_START);
        add(jScrollPane, BorderLayout.CENTER);
        //add(jScrollPane);
    }


    //======================== ADBConnector.CallBack===============================
    @Override
    public void deviceChanged(IDevice iDevice, int changeMask) {
        Logger.d(this, "deviceChanged["+changeMask+"] "+iDevice.toString());
        if (changeMask == IDevice.CHANGE_STATE || iDevice.isOnline()) {
            //replace this device in device list
            Constants.CONNECTED_DEVICES.put(iDevice.getSerialNumber(), iDevice);


            if (Constants.DEVICE_STATE.get(iDevice.getSerialNumber()) == null) {
                Constants.DEVICE_STATE.put(iDevice.getSerialNumber(), false);

            }

            //update list of connected devices
            deviceList();
        }
    }

    private void deviceList(){
        //clean all checkboxes on panel
        deviceListPanel.removeAll();
        batteryLabels.clear();
        //set layout
        deviceListPanel.setLayout(new BoxLayout(deviceListPanel, BoxLayout.Y_AXIS));

        for (Map.Entry<String, IDevice> pairs : Constants.CONNECTED_DEVICES.entrySet()) {


            deviceListPanel.add(prepareSingleDevicePanel(pairs));
        }


        deviceListPanel.revalidate();
        deviceListPanel.repaint();
    }

    private JPanel prepareSingleDevicePanel(Map.Entry<String, IDevice> pairs){


        String deviceName = null;
        if(pairs.getValue().isEmulator()){
            deviceName = pairs.getValue().getAvdName();
        }else{
            deviceName = pairs.getValue().getProperty("ro.product.manufacturer")
                    + " " + pairs.getValue().getProperty("ro.product.model");
        }

        if(!Util.isEmpty(deviceName) && deviceName.length()>20){
            deviceName = deviceName.substring(0,20).concat("...");
        }

        String title = "<html><strong>"
                + deviceName
                + "</strong>(" + pairs.getValue().getProperty("ro.build.version.release") + ")"
                + "<br>" + pairs.getValue().getSerialNumber() + "</html>";

        CustomJCheckBox<IDevice> checkbox = new CustomJCheckBox<IDevice>();
        checkbox.setTag(pairs.getValue());
        checkbox.addItemListener(this);



        //check previous state from state hashmap
        boolean checked = Constants.DEVICE_STATE.get(pairs.getValue().getSerialNumber());
        checkbox.setSelected(checked);


        JPanel jPanel = new JPanel();
        jPanel.setLayout(new BorderLayout());
        jPanel.setBorder(new EmptyBorder(0, 0, 0, 5));
        jPanel.setMaximumSize(new Dimension(500, 50));
        //jPanel.setMinimumSize(new Dimension(120, 40));
        jPanel.setPreferredSize(new Dimension(120, 40));



        JLabel label = new JLabel(title);
        label.setMaximumSize(new Dimension(50,100));
        label.setPreferredSize(new Dimension(50, 50));
        label.setMinimumSize(new Dimension(50, 50));

        Icon normalIcon = IconLoader.getIcon(Constants.IMAGE_FOLDER_PATH + "ic_info.png");
        Icon overIcon = IconLoader.getIcon(Constants.IMAGE_FOLDER_PATH + "ic_info_over.png");
        Icon pressedIcon = IconLoader.getIcon(Constants.IMAGE_FOLDER_PATH + "ic_info_pressed.png");
        final JXButton<IDevice> infoButton = new JXButton<IDevice>(normalIcon, pressedIcon, overIcon);
        infoButton.setTag(pairs.getValue());
        infoButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                IDevice device = infoButton.getTag();
                if (device != null) {

                    new DeviceInfoDialog(device);
                }
            }
        });

        Box buttonsBox = Box.createHorizontalBox();


        JXLabel<IDevice> batteryStateLabel = new JXLabel<IDevice>();
        batteryStateLabel.setTag(pairs.getValue());
        batteryLabels.add(batteryStateLabel);
        String batteryLevel = null;
        try {
            batteryLevel = String.valueOf(pairs.getValue().getBatteryLevel(60000));
        } catch (TimeoutException e) {
            e.printStackTrace();
        } catch (AdbCommandRejectedException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ShellCommandUnresponsiveException e) {
            e.printStackTrace();
        }

        if(!Util.isEmpty(batteryLevel)){
            batteryStateLabel.setText(batteryLevel+"%");


        }else{
            batteryStateLabel.setText("N/A");
        }

        buttonsBox.add(Box.createVerticalGlue());
        buttonsBox.add(batteryStateLabel);
        buttonsBox.add(Box.createRigidArea(new Dimension(5,0)));

        buttonsBox.add(Box.createVerticalGlue());
        buttonsBox.add(infoButton);



        //Buttons
        Box box = Box.createHorizontalBox();

        box.add(buttonsBox);

        jPanel.add(checkbox, BorderLayout.WEST);
        jPanel.add(label, BorderLayout.CENTER);
        jPanel.add(box, BorderLayout.EAST);
        return jPanel;
    }

    @Override
    public void deviceDisconnected(IDevice iDevice) {
        Logger.d(this, "deviceDisconnected "+iDevice.toString());
        //replace this device in device list
        Constants.CONNECTED_DEVICES.remove(iDevice.getSerialNumber());

        //update list of connected devices
        deviceList();
    }

    @Override
    public void deviceConnected(IDevice iDevice) {
        Logger.d(this, "deviceConnected "+iDevice.toString());
    }

    @Override
    public void bridgeChanged(AndroidDebugBridge androidDebugBridge) {
        Logger.d(this, "bridgeChanged");
    }

    @Override
    public void clientChanged(Client client, int changeMask) {
        Logger.d(this, "clientChanged["+changeMask+"] "+client.toString());
    }

    @Override
    @SuppressWarnings("unchecked")
    public void itemStateChanged(ItemEvent e) {
        if (e.getItem() instanceof CustomJCheckBox) {
            CustomJCheckBox<IDevice> customJCheckBox = (CustomJCheckBox<IDevice>) e.getItem();
            IDevice iDevice = customJCheckBox.getTag();
            if (iDevice != null) {
                Constants.DEVICE_STATE.put(iDevice.getSerialNumber(), ((JCheckBox) e.getItem()).isSelected());
            }

        }
    }

    @Override
    public void batteryLevel(IDevice device, final int level) {
        Logger.d(this, device.toString()+" ["+level+"]");
        if(!batteryLabels.isEmpty()){
            for(final JXLabel<IDevice> label: batteryLabels){
                if(label.getTag().getSerialNumber().equals(device.getSerialNumber())){
                    SwingUtilities.invokeLater(new Runnable() {
                        @Override
                        public void run() {
                            label.setText(String.valueOf(level)+"%");
                        }
                    });

                }
            }
        }

    }

    //==============================================================================

}
