package com.remoterapp.adb.ui.panels;

import com.android.ddmlib.AdbCommandRejectedException;
import com.android.ddmlib.IDevice;
import com.android.ddmlib.ShellCommandUnresponsiveException;
import com.android.ddmlib.TimeoutException;
import com.intellij.openapi.util.IconLoader;
import com.intellij.ui.components.JBScrollPane;
import com.intellij.ui.components.JBTabbedPane;
import com.intellij.util.ui.UIUtil;
import com.remoterapp.adb.components.JXButton;
import com.remoterapp.adb.persistance.Macro;
import com.remoterapp.adb.ui.MacroDialog;
import com.remoterapp.adb.ui.MainUI;

import com.remoterapp.adb.util.Constants;
import com.remoterapp.adb.util.Logger;
import com.remoterapp.adb.util.ShellOutputReceiverImpl;
import com.remoterapp.adb.util.Util;

import javax.swing.*;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.Map;

/**
 * TODO Add description for class
 *
 * @author Rob Slama
 * @version 1
 */
public class OperationsPanelUI extends JPanel{


    private JPanel macroListPanel = new JPanel();
    private JTextArea textArea, shellTextArea;

    public OperationsPanelUI(){
        init();

    }

    private void init(){

        JButton addNewMacroButton = new javax.swing.JButton();

        JBTabbedPane operationPanel = new JBTabbedPane();

        JButton executeScriptButton = new javax.swing.JButton();
        JPanel macrosPanel = new javax.swing.JPanel();
        JButton sendTextButton = new javax.swing.JButton();
        JPanel sendTextPanel = new JPanel();
        JPanel shellPanel = new javax.swing.JPanel();

        operationPanel.setBackground(UIUtil.getPanelBackground());
        operationPanel.setMinimumSize(new Dimension(200, 200));
        operationPanel.setMaximumSize(new Dimension(400, 500));
        operationPanel.setPreferredSize(new Dimension(300, 200));


        setMinimumSize(new Dimension(200, 200));
        setMaximumSize(new Dimension(400, 500));
        setPreferredSize(new Dimension(300, 200));

        textArea = new JTextArea();
        shellTextArea = new JTextArea();


        operationPanel.setBorder(
                new CompoundBorder( new EmptyBorder(10,10,10,10),
                new javax.swing.border.LineBorder(Color.gray, 1, true)));

        sendTextButton.setText("SEND");
        sendTextButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sendTextData();
            }
        });


        textArea.setColumns(20);
        //textArea.setRows(5);
        textArea.setWrapStyleWord(true);
        textArea.setLineWrap(true);
        textArea.setBorder(new LineBorder(Color.GRAY, 1));
        JBScrollPane textAreaScrollPane = new JBScrollPane(textArea);
        textAreaScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        textAreaScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);

        //sendTextPanel.setBorder(new EmptyBorder(10,10,10,10));
        sendTextPanel.setLayout(new BorderLayout());

        Box butonBox = Box.createHorizontalBox();
        butonBox.add(Box.createHorizontalGlue());
        butonBox.add(sendTextButton);

        sendTextPanel.add(butonBox, BorderLayout.PAGE_END);
        sendTextPanel.add(textAreaScrollPane, BorderLayout.CENTER);

        operationPanel.addTab("input text", sendTextPanel);


        //setup macro part

        addNewMacroButton.setText("ADD");

        addNewMacroButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {

                new MacroDialog(null, OperationsPanelUI.this);
            }
        });

        //TODO
        JBScrollPane macroScrollPanel = new JBScrollPane(macroListPanel);
        macroScrollPanel.setViewportView(macroListPanel);
        macroScrollPanel.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        macroScrollPanel.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);

        macroListPanel.setBorder(new LineBorder(Color.gray, 1));
        macroListPanel.setLayout(new BorderLayout());
        butonBox = Box.createHorizontalBox();
        butonBox.add(Box.createHorizontalGlue());
        butonBox.add(addNewMacroButton);

        macrosPanel.setLayout(new BorderLayout());
        macrosPanel.add(macroScrollPanel, BorderLayout.CENTER);
        macrosPanel.add(butonBox, BorderLayout.PAGE_END);

        operationPanel.addTab("macros", macrosPanel);

        executeScriptButton.setText("EXECUTE");
        executeScriptButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                performShellCommand();
            }
        });


        shellTextArea.setColumns(20);
        //shellTextArea.setRows(5);
        shellTextArea.setWrapStyleWord(true);
        shellTextArea.setLineWrap(true);
        JBScrollPane shellAreaScrollPane = new JBScrollPane(shellTextArea);
        shellAreaScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        shellAreaScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);


        shellAreaScrollPane.setBorder(new LineBorder(Color.GRAY, 1));
        shellPanel.setLayout(new BorderLayout());
        shellPanel.setLayout(new BorderLayout());

        butonBox = Box.createHorizontalBox();
        butonBox.add(Box.createHorizontalGlue());
        butonBox.add(executeScriptButton);

        shellPanel.add(shellAreaScrollPane, BorderLayout.CENTER);
        shellPanel.add(butonBox, BorderLayout.PAGE_END);

        operationPanel.addTab("shell", shellPanel);
        setLayout(new BorderLayout());
        add(operationPanel, BorderLayout.CENTER);

    }


    private void performShellCommand() {
        if (shellTextArea.getText() != null && shellTextArea.getText().length() > 0) {
            String content = parseTextForMacro(shellTextArea.getText());
            if(!Util.isEmpty(content)){
                sendCommandToAllDevice(content, SHELL_COMMAND);
            }

        }

    }

    private String parseTextForMacro(String text) {
        StringBuffer stringBuffer = new StringBuffer();
        String lines[] = text.split("\\r?\\n");

        if(lines!=null && lines.length>0){
            for(String line:lines){
                String tokens[] = line.split(" ");
                if(tokens!=null && tokens.length>0 && tokens[0].startsWith("$")){
                    stringBuffer.append(executeMarcoInjection(tokens));
                    stringBuffer.append("\n");
                }else if(tokens!=null && tokens.length>0){
                    stringBuffer.append(line);
                    stringBuffer.append("\n");
                }
            }
            return stringBuffer.toString();
        }
        return null;
    }

    public String executeMarcoInjection(String [] injectTokens){
        //search macro
        if (Constants.PREFERENCE_BEAN != null) {
            String macroName = injectTokens[0].substring(1);
            for (Map.Entry<String, Macro> entry : Constants.PREFERENCE_BEAN.getMacrosMap().entrySet()) {
                if(entry.getValue()!=null && !Util.isEmpty(entry.getValue().getName())){
                    if(entry.getValue().getName().equals(macroName)){
                        Logger.d(this, "macroFounded >>"+entry.getValue().toString());
                        String [] macroValues = entry.getValue().getContent().split(" ");
                        String newContent = "";
                        if(entry.getValue().isInputTextMacro()){
                              newContent = "input text ";
                        }
                         newContent = newContent+entry.getValue().getContent();
                        if(macroValues!=null && macroValues.length>0){
                            int a = 1;

                            for(String token: macroValues){
                                if(token.startsWith("$")){
                                    if(a<(injectTokens.length)){
                                        newContent = newContent.replace(token, injectTokens[a++]);
                                    }else{
                                        newContent = newContent.replace(token, token.substring(1));
                                    }

                                }
                            }
                        }
                        return newContent;

                    }
                }

            }
        }
        return "";
    }


    private static final int INPUT_TEXT_COMMAND = 0;
    private static final int SHELL_COMMAND = 1;

    private void sendCommandToAllDevice(final String text, final int commandType) {
        Logger.d(this, "commandType[" + commandType + "] content[" + text + "]");
        for (Map.Entry<String, Boolean> entry : Constants.DEVICE_STATE.entrySet()) {
            if (entry.getValue()) {
                final IDevice iDevice = Util.findDeviceBySerial(Constants.CONNECTED_DEVICES, entry.getKey());
                if (iDevice != null && iDevice.isOnline()) {


                    new Thread() {
                        @Override
                        public void run() {
                            super.run();
                            try {
                                if (INPUT_TEXT_COMMAND == commandType) {
                                    final String command = "input text " + text;

                                    iDevice.executeShellCommand(command, new ShellOutputReceiverImpl(iDevice, ""));
                                } else if (SHELL_COMMAND == commandType) {
                                    iDevice.executeShellCommand(text, new ShellOutputReceiverImpl(iDevice, text));
                                }

                            } catch (TimeoutException e1) {
                                e1.printStackTrace();
                            } catch (AdbCommandRejectedException e1) {
                                e1.printStackTrace();
                            } catch (ShellCommandUnresponsiveException e1) {
                                e1.printStackTrace();
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }
                        }
                    }.start();

                }
            }
        }

    }

    private void sendTextData() {
        String textAreaValue = textArea.getText();
        if (textAreaValue == null || textAreaValue.length() == 0) {
            return;
        }
        final StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("'");
        stringBuilder.append(Util.formatText(textAreaValue));
        stringBuilder.append("'");

        sendCommandToAllDevice(stringBuilder.toString(), INPUT_TEXT_COMMAND);


    }

    public static void updateMacroPanel() {
        for (MainUI mainUI : MainUI.getInstances().values()) {
            final OperationsPanelUI panelUI;
            if((panelUI = mainUI.getOperationsPanelUI())!=null){
                panelUI.prepareMacroPanel();
            }
        }
    }

    public void prepareMacroPanel() {
        macroListPanel.removeAll();

        Box box = Box.createVerticalBox();

        if (Constants.PREFERENCE_BEAN != null) {

            for (Map.Entry<String, Macro> entry : Constants.PREFERENCE_BEAN.getMacrosMap().entrySet()) {
                box.add(createMacroPanel(macroListPanel, entry.getValue()));
                box.add(Box.createRigidArea(new Dimension(0, 10)));

            }
        }


        macroListPanel.add(box, BorderLayout.CENTER);
        macroListPanel.revalidate();
        macroListPanel.repaint();

    }


    private JPanel createMacroPanel(JPanel parent, final Macro macro) {
        JPanel jPanel = new JPanel();
        jPanel.setLayout(new BorderLayout());
        jPanel.setBorder(new EmptyBorder(5, 10, 5, 10));
        jPanel.setBackground(parent.getBackground().darker());
        jPanel.setMaximumSize(new Dimension(500, 40));
        String labelText = macro.getName();

        if (!Util.isEmpty(labelText) && labelText.length() > 20) {
            labelText = labelText.substring(0, 20).concat("...");
        }

        JLabel label = new JLabel("<html><strong>" + labelText + "</strong></html>");

        Icon normalIcon = IconLoader.findIcon(Constants.IMAGE_FOLDER_PATH + "ic_delete_macro.png");
        Icon overIcon = IconLoader.findIcon(Constants.IMAGE_FOLDER_PATH + "ic_delete_macro_over.png");
        Icon pressedIcon = IconLoader.findIcon(Constants.IMAGE_FOLDER_PATH + "ic_delete_macro_pressed.png");
        JXButton<Macro> deleteButton = new JXButton<Macro>(normalIcon, pressedIcon, overIcon);
        deleteButton.setTag(macro);
        deleteButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                Constants.PREFERENCE_BEAN.removeMacro(macro);
                Constants.PREFERENCE_BEAN.save();
                updateMacroPanel();
            }
        });

        normalIcon = IconLoader.getIcon(Constants.IMAGE_FOLDER_PATH + "ic_edit_macro.png");
        overIcon = IconLoader.getIcon(Constants.IMAGE_FOLDER_PATH + "ic_edit_macro_over.png");
        pressedIcon = IconLoader.getIcon(Constants.IMAGE_FOLDER_PATH + "ic_edit_macro_pressed.png");
        final JXButton<Macro> editButton = new JXButton<Macro>(normalIcon, pressedIcon, overIcon);
        editButton.setTag(macro);
        editButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                new MacroDialog(editButton.getTag(), OperationsPanelUI.this);
            }
        });

        normalIcon = IconLoader.getIcon(Constants.IMAGE_FOLDER_PATH + "ic_run_macro.png");
        overIcon = IconLoader.getIcon(Constants.IMAGE_FOLDER_PATH + "ic_run_macro_over.png");
        pressedIcon = IconLoader.getIcon(Constants.IMAGE_FOLDER_PATH + "ic_run_macro_pressed.png");
        final JXButton<Macro> runButton = new JXButton<Macro>(normalIcon, pressedIcon, overIcon);
        runButton.setTag(macro);
        runButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                Macro macro = runButton.getTag();
                if (macro != null) {
                    String content = macro.getContent().replace("$","");

                    if (macro.isShellCommandMacro()) {
                        sendCommandToAllDevice(content, SHELL_COMMAND);
                    } else if (macro.isInputTextMacro()) {
                        sendCommandToAllDevice(content, INPUT_TEXT_COMMAND);
                    }
                }
            }
        });

        Box buttonsBox = Box.createHorizontalBox();

        buttonsBox.add(deleteButton);
        buttonsBox.add(Box.createRigidArea(new Dimension(10, 0)));
        buttonsBox.add(editButton);
        buttonsBox.add(Box.createRigidArea(new Dimension(20, 0)));
        buttonsBox.add(runButton);

        //Buttons
        Box box = Box.createHorizontalBox();
        box.add(buttonsBox);


        jPanel.add(label, BorderLayout.CENTER);
        jPanel.add(box, BorderLayout.EAST);
        return jPanel;
    }

    public void saveMacroAndReloadPanel(Macro macro) {

        if (macro != null) {
            Constants.PREFERENCE_BEAN.addMacro(macro);
            Constants.PREFERENCE_BEAN.save();
            updateMacroPanel();

        }
    }
}
