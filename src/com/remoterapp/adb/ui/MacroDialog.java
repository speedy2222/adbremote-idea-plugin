package com.remoterapp.adb.ui;

import com.intellij.openapi.ui.DialogWrapper;
import com.remoterapp.adb.components.JTextFieldLimit;
import com.remoterapp.adb.util.Constants;
import com.remoterapp.adb.util.Util;
import com.remoterapp.adb.ui.panels.OperationsPanelUI;
import org.jetbrains.annotations.Nullable;
import com.remoterapp.adb.persistance.Macro;

import javax.swing.*;
import java.awt.*;

/**
 *
 * @author Rob Slama
 * @version 1
 */
public class MacroDialog extends DialogWrapper {

    private Macro macro;
    private OperationsPanelUI operationsPanelUI;

    public MacroDialog(Macro macro, OperationsPanelUI operationsPanelUI) {
        super(Constants.PROJECT, false);
        this.macro = macro;
        this.operationsPanelUI = operationsPanelUI;
        setTitle("Create New Macro");
        init();
        fillData();
        show();
    }



    @Nullable
    @Override
    protected JComponent createCenterPanel() {
        return createContent();
    }

    @Override
    public void doCancelAction() {
        super.doCancelAction();
    }

    @Override
    protected void doOKAction() {

        //validation
        if (!Util.isEmpty(macroNameTextField.getText()) && !Util.isEmpty(macroContentTextArea.getText())) {
            if (macro == null) {
                macro = new Macro();
            }
            macro.setContent(macroContentTextArea.getText());
            macro.setName(macroNameTextField.getText());
            macro.setInputTextMacro(inputTextButton.isSelected());
            macro.setShellCommandMacro(shellButton.isSelected());
            //save
            MacroDialog.this.operationsPanelUI.saveMacroAndReloadPanel(macro);
            MacroDialog.this.dispose();

        }
    }



    private JPanel createContent(){
        JPanel panel = new JPanel();
        buttonGroup1 = new javax.swing.ButtonGroup();

        macroNameLabel = new javax.swing.JLabel();
        macroNameTextField = new javax.swing.JTextField();
        macroContentLabel = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        macroContentTextArea = new javax.swing.JTextArea();
        macroTypeLabel = new javax.swing.JLabel();
        buttonGroup = new ButtonGroup();
        inputTextButton = new JRadioButton("input text type");
        shellButton = new JRadioButton("shell command type");



        panel.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());


        macroNameLabel.setText("macro name");
        macroNameLabel.setFont(new java.awt.Font("Lucida Grande", Font.BOLD, 12));
        panel.add(macroNameLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 0, -1, -1));

        macroNameTextField.setDocument(new JTextFieldLimit(50));
        panel.add(macroNameTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 20, 280, -1));

        macroContentLabel.setText("macro content");
        macroContentLabel.setFont(new java.awt.Font("Lucida Grande", Font.BOLD, 12));
        panel.add(macroContentLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 60, -1, -1));

        macroContentTextArea.setColumns(20);
        macroContentTextArea.setRows(5);
        macroContentTextArea.setLineWrap(true);
        macroContentTextArea.setWrapStyleWord(true);
        jScrollPane1.setViewportView(macroContentTextArea);

        panel.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 80, 280, -1));

        macroTypeLabel.setText("macro type");
        macroTypeLabel.setFont(new java.awt.Font("Lucida Grande", Font.BOLD, 12));
        panel.add(macroTypeLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 180, -1, -1));

        buttonGroup.add(inputTextButton);
        buttonGroup.add(shellButton);
        inputTextButton.setSelected(true);
        panel.add(inputTextButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 200, -1, -1));
        panel.add(shellButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 220, -1, -1));

        return panel;
    }

    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel macroContentLabel;
    private javax.swing.JTextArea macroContentTextArea;
    private javax.swing.JLabel macroNameLabel;
    private javax.swing.JTextField macroNameTextField;
    private javax.swing.JLabel macroTypeLabel;
    private ButtonGroup buttonGroup;
    private JRadioButton inputTextButton;
    private JRadioButton shellButton;
    private javax.swing.JLabel titleLabel;

    private void fillData(){
        if(this.macro==null){
            return;
        }
        macroNameTextField.setText(macro.getName());
        macroContentTextArea.setText(macro.getContent());
        if(macro.isInputTextMacro()){
            inputTextButton.setSelected(true);
        }else{
            inputTextButton.setSelected(false);
        }

        if(macro.isShellCommandMacro()){
            shellButton.setSelected(true);
        }else{
            shellButton.setSelected(false);
        }

    }

}
