package com.remoterapp.adb.ui;

import com.intellij.openapi.actionSystem.*;
import com.intellij.openapi.editor.Document;
import com.intellij.openapi.fileEditor.FileDocumentManager;
import com.intellij.openapi.fileTypes.FileTypeManager;
import com.intellij.openapi.fileTypes.StdFileTypes;
import com.intellij.testFramework.LightVirtualFile;
import com.intellij.ui.components.JBScrollPane;
import com.remoterapp.adb.components.SplitModel;
import com.remoterapp.adb.ui.panels.ConnectedDevicesUI;
import com.remoterapp.adb.ui.panels.OperationsPanelUI;
import com.remoterapp.adb.ui.panels.SettingsPanelUI;
import com.remoterapp.adb.ui.panels.VirtualKeysPanelUI;
import org.jdesktop.swingx.JXMultiSplitPane;

import javax.swing.*;
import java.awt.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;

/**
 * TODO Add description for class
 *
 * @author Rob Slama
 * @version 1
 */
public class MainUI extends JPanel {

    private static final HashMap<MainUI, MainUI> instances = new HashMap<MainUI, MainUI>();
    private ConnectedDevicesUI connectedDevicesUI;
    private OperationsPanelUI operationsPanelUI;
    private VirtualKeysPanelUI virtualKeysPanelUI;
    private SettingsPanelUI settingsPanelUI;




    public MainUI(){
        //add this instance to internal list for update UI on all opened instances of IDE
        instances.put(this,this);

         setLayout(new BorderLayout());

        //init virtualfile and document
        final DefaultActionGroup toolbarActions = createToolbarActions();
        final ActionToolbar toolbar = ActionManager.getInstance().createActionToolbar(ActionPlaces.UNKNOWN, toolbarActions, true);
        //add(toolbar.getComponent(), BorderLayout.NORTH);
        initComponents();
    }

    private DefaultActionGroup createToolbarActions() {
        final ActionManager actionManager = ActionManager.getInstance();
        final DefaultActionGroup group = new DefaultActionGroup();




        group.add(actionManager.getAction("ADBRemote.OpenFileCommander"));


        return group;
    }



    public static HashMap<MainUI,MainUI> getInstances(){
        return instances;
    }

    private void initComponents(){
        connectedDevicesUI = new ConnectedDevicesUI();
        operationsPanelUI = new OperationsPanelUI();
        OperationsPanelUI.updateMacroPanel();
        virtualKeysPanelUI = new VirtualKeysPanelUI();
        settingsPanelUI = new SettingsPanelUI();


        setMinimumSize(new java.awt.Dimension(400, 300));
        setSize(new java.awt.Dimension(400, 300));

        //setLayout(new BorderLayout());

        JPanel mainPane = new JPanel();
        mainPane.setLayout(new BoxLayout(mainPane, BoxLayout.PAGE_AXIS));

        Box verticalBox = Box.createVerticalBox();
        verticalBox.add(connectedDevicesUI);

        verticalBox.add(operationsPanelUI);
        verticalBox.add(virtualKeysPanelUI);
        verticalBox.add(settingsPanelUI);


        mainPane.add(verticalBox);

        JXMultiSplitPane jSplitPane = new JXMultiSplitPane();
        jSplitPane.setModel(new SplitModel());
        jSplitPane.setDividerPainter(new SplitModel.BevelDividerPainter(jSplitPane));

        jSplitPane.add(connectedDevicesUI, SplitModel.P1);
        jSplitPane.add(operationsPanelUI, SplitModel.P2);
        jSplitPane.add(virtualKeysPanelUI, SplitModel.P3);
        jSplitPane.add(settingsPanelUI, SplitModel.P4);

        JBScrollPane mainScrollPanel = new JBScrollPane();
        mainScrollPanel.setAlignmentY(Component.TOP_ALIGNMENT);
        mainScrollPanel.getViewport().add(jSplitPane);
        add(mainScrollPanel);
    }

    public OperationsPanelUI getOperationsPanelUI(){
        return this.operationsPanelUI;
    }

}
