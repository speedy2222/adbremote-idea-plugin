package com.remoterapp.adb.ui.dialogs;

import com.android.ddmlib.*;
import com.intellij.history.utils.RunnableAdapter;
import com.intellij.openapi.progress.ProgressIndicator;
import com.intellij.openapi.progress.ProgressManager;
import com.intellij.openapi.progress.Task;
import com.intellij.openapi.progress.util.ProgressIndicatorBase;
import com.intellij.openapi.progress.util.ProgressWindow;
import com.intellij.openapi.progress.util.ProgressWindowWithNotification;
import com.intellij.openapi.ui.DialogWrapper;
import com.intellij.openapi.util.IconLoader;
import com.intellij.ui.components.JBScrollPane;
import com.intellij.ui.components.JBTabbedPane;
import com.intellij.ui.table.JBTable;
import com.remoterapp.adb.components.JXButton;
import com.remoterapp.adb.persistance.Macro;
import com.remoterapp.adb.util.Constants;
import com.remoterapp.adb.util.Logger;
import com.remoterapp.adb.util.PackageParserShellOutupReceiver;
import com.remoterapp.adb.util.Util;
import org.jdesktop.swingx.painter.AbstractLayoutPainter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.TableModelListener;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * TODO Add description for class
 *
 * @author Rob Slama
 * @version 1
 */
public class DeviceInfoDialog extends DialogWrapper {

    private final HashMap<String, String> deviceProperties = new HashMap<String, String>();
    private Object[][] propertiesData;//, installedApplicationsData;
    private final ArrayList<String> installedApplicationsData = new ArrayList<String>();
    private final ArrayList<JXButton<String>> uninstallButtons = new ArrayList<JXButton<String>>();
    private JBTable applicationListTable;
    private DefaultTableModel installedAppsTableModel;

    private JBScrollPane installedApplicationPanel;
    private IDevice device;
    private JComponent component;

    public DeviceInfoDialog(final IDevice device) {
        super(Constants.PROJECT, true);
        this.device = device;
        loadApps();
        String deviceName = null;
        if (device.isEmulator()) {
            deviceName = device.getAvdName();
        } else {
            deviceName = device.getProperty("ro.product.manufacturer")
                    + " " + device.getProperty("ro.product.model");
        }
        setTitle(deviceName);
        deviceProperties.putAll(device.getProperties());
        propertyToArray();


        init();
        setModal(true);
        show();
    }

    @Nullable
    @Override
    protected JComponent createCenterPanel() {
        return component = initDialogContent();
    }


    private JPanel initDialogContent() {
        JPanel panel = new JPanel(new BorderLayout());

        JBTabbedPane tabbedPane = new JBTabbedPane();
        tabbedPane.addTab("Device properties", properties());
        tabbedPane.addTab("Installed Apps", installedApplicationPanel = installedApps());

        panel.add(tabbedPane, BorderLayout.CENTER);
        panel.add(installAPKPanel(), BorderLayout.SOUTH);
        return panel;
    }

    public synchronized void packageUpdate(ArrayList<String> packageNames) {
        if (packageNames != null) {


            for (String s : packageNames) {
                installedApplicationsData.add(s);
                Icon normalIcon = IconLoader.findIcon(Constants.IMAGE_FOLDER_PATH + "ic_delete_macro.png");
                Icon overIcon = IconLoader.findIcon(Constants.IMAGE_FOLDER_PATH + "ic_delete_macro_over.png");
                Icon pressedIcon = IconLoader.findIcon(Constants.IMAGE_FOLDER_PATH + "ic_delete_macro_pressed.png");
                final JXButton<String> uninstallButton = new JXButton<String>(normalIcon, pressedIcon, overIcon);
                uninstallButton.setTag(s);
                uninstallButton.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent event) {
                        if (device != null) {
                            Task.Modal modal = new Task.Modal(Constants.PROJECT,"uninstall "+uninstallButton.getTag(), false) {
                                @Override
                                public void run(@NotNull ProgressIndicator progressIndicator) {
                                    try {
                                        device.uninstallPackage(uninstallButton.getTag());
                                        installedApplicationsData.remove(uninstallButton.getTag());
                                        uninstallButtons.remove(uninstallButton);
                                    } catch (InstallException e) {
                                        e.printStackTrace();
                                    }


                                    reloadInstalledAppsPanel();
                                }
                            };
                            ProgressManager.getInstance().run(modal);


                        }

                    }
                });
                uninstallButtons.add(uninstallButton);
                Logger.d(this, s);

            }
            reloadInstalledAppsPanel();

        }
    }

    @NotNull
    @Override
    protected Action[] createActions() {
        Action[] actions = super.createActions();
        Action[] newActions = new Action[1];
        if (actions != null) {
            for (Action action : actions) {
                if (action instanceof OkAction) {

                    newActions[0] = action;
                }
            }
        }
        return newActions;
    }

    private void propertyToArray() {
        propertiesData = new Object[deviceProperties.size()][2];
        Iterator<Map.Entry<String, String>> entries = deviceProperties.entrySet().iterator();
        int a = 0;
        while (entries.hasNext()) {
            Map.Entry<String, String> pair = entries.next();
            propertiesData[a][0] = pair.getKey();
            propertiesData[a][1] = pair.getValue();
            ++a;
        }
    }

    private JBScrollPane installedApps() {
        applicationListTable = new JBTable(installedAppsTableModel = new DefaultTableModel() {


            private static final String COLUMN_PACKAGE_NAME = "Application Package";
            private static final String COLUMN_UNINSTALL = "";

            @Override
            public int getRowCount() {
                if (installedApplicationsData == null) {
                    return 0;
                } else {
                    return installedApplicationsData.size();
                }

            }

            @Override
            public int getColumnCount() {
                return 2;
            }

            @Override
            public String getColumnName(int i) {
                switch (i) {
                    case 0:
                        return COLUMN_PACKAGE_NAME;
                    case 1:
                        return COLUMN_UNINSTALL;

                }
                return null;
            }

            @Override
            public Class<?> getColumnClass(int c) {
                return getValueAt(0, c).getClass();
            }

            @Override
            public boolean isCellEditable(int col, int row) {
                switch (col) {
                    case 0:
                        return false;
                    case 1:
                        return true;
                }
                return false;
            }

            @Override
            public Object getValueAt(int row, int col) {
                if (col == 0) {
                    return installedApplicationsData.get(row);
                } else if (col == 1) {
                    return uninstallButtons.get(row);
                }
                return null;


            }

            @Override
            public void setValueAt(Object o, int i, int i2) {

            }

            @Override
            public void addTableModelListener(TableModelListener tableModelListener) {

            }

            @Override
            public void removeTableModelListener(TableModelListener tableModelListener) {

            }
        });
        applicationListTable.getColumnModel().getColumn(1).setCellRenderer(new JButtonRenderer());
        applicationListTable.getColumnModel().getColumn(1).setWidth(40);
        applicationListTable.getColumnModel().getColumn(1).setMaxWidth(40);
        applicationListTable.setRowHeight(35);
        applicationListTable.setRowSelectionAllowed(true);
        applicationListTable.setColumnSelectionAllowed(true);
        applicationListTable.setCellSelectionEnabled(true);
        applicationListTable.addMouseListener(new JTableButtonMouseListener(applicationListTable));


        JBScrollPane pane = new JBScrollPane(applicationListTable);


        return pane;
    }

    private JBScrollPane properties() {
        JBTable jTable = new JBTable(new TableModel() {

            private static final String COLUMN_PROPERTY_NAME = "Property Name";
            private static final String COLUMN_PROPERTY_VALUE = "Property Value";


            @Override
            public int getRowCount() {
                return propertiesData.length;
            }

            @Override
            public int getColumnCount() {
                return 2;
            }

            @Override
            public String getColumnName(int i) {
                switch (i) {
                    case 0:
                        return COLUMN_PROPERTY_NAME;
                    case 1:
                        return COLUMN_PROPERTY_VALUE;

                }
                return null;
            }

            @Override
            public Class<?> getColumnClass(int c) {
                return getValueAt(0, c).getClass();
            }

            @Override
            public boolean isCellEditable(int i, int i2) {
                return false;
            }

            @Override
            public Object getValueAt(int row, int col) {
                return propertiesData[row][col];
            }

            @Override
            public void setValueAt(Object o, int i, int i2) {

            }

            @Override
            public void addTableModelListener(TableModelListener tableModelListener) {

            }

            @Override
            public void removeTableModelListener(TableModelListener tableModelListener) {

            }
        });
        jTable.setRowSelectionAllowed(true);
        jTable.setColumnSelectionAllowed(true);
        jTable.setCellSelectionEnabled(true);
        JBScrollPane pane = new JBScrollPane(jTable);
        return pane;
    }


    class JButtonRenderer extends DefaultTableCellRenderer {

        JButtonRenderer() {

            setOpaque(true);
        }

        @Override
        public Component getTableCellRendererComponent(JTable jTable, Object o, boolean b, boolean b2, int i, int i2) {
            JButton button = (JButton) o;
            return button;
        }
    }

    private class JTableButtonMouseListener extends MouseAdapter {
        private final JTable table;

        public JTableButtonMouseListener(JTable table) {
            this.table = table;
        }

        @Override
        public void mouseClicked(MouseEvent e) {
            int column = table.getColumnModel().getColumnIndexAtX(e.getX());
            int row = e.getY() / table.getRowHeight();

            if (row < table.getRowCount() && row >= 0 && column < table.getColumnCount() && column >= 0) {
                Object value = table.getValueAt(row, column);
                if (value instanceof JXButton) {
                    JXButton button = (JXButton) value;
                    button.doClick();
                    Logger.d(this, "mouseClicked");
                }
            }
        }


    }

    private FileFilter getFileFilter() {
        FileFilter filter = new FileNameExtensionFilter("APK file", "apk");
        return filter;
    }

    private JPanel installAPKPanel() {
        final JPanel panel = new JPanel(new BorderLayout());

        TitledBorder titledBorder = BorderFactory.createTitledBorder("Select APK file to install");
        titledBorder.setBorder(new CompoundBorder(new EmptyBorder(5, 5, 5, 5),
                new EtchedBorder(EtchedBorder.LOWERED)));
        titledBorder.setTitleJustification(TitledBorder.LEFT);
        panel.setBorder(titledBorder);

        installAPKPathTextField = new JTextField();
        openFileChooserButton = new javax.swing.JButton();

        installAPKButton = new JButton("Install APK");
        installAPKButton.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                performInstallApplication();
            }
        });
        Box box = Box.createHorizontalBox();
        box.setAlignmentX(Component.RIGHT_ALIGNMENT);
        box.add(Box.createHorizontalGlue());
        box.add(installAPKButton);


        openFileChooserButton.setText("...");
        openFileChooserButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                performOpenFileChooser(panel);
            }
        });


        panel.add(installAPKPathTextField, BorderLayout.CENTER);
        panel.add(openFileChooserButton, BorderLayout.EAST);
        panel.add(box, BorderLayout.SOUTH);

        return panel;
    }

    private void performOpenFileChooser(JPanel panel) {
        final FileDialog fc = new FileDialog(new JDialog(),"Select file", FileDialog.LOAD);
        fc.setModal(true);
        fc.setVisible(true);

        /*fc.setFilenameFilter(new FilenameFilter() {
            @Override
            public boolean accept(File file, String name) {
                return (name.endsWith(".apk") || name.endsWith(".APK"));
            }
        });*/
        String directory = fc.getDirectory();
        String file = fc.getFile();
        installAPKPathTextField.setText(directory+file);
        /*fc.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {

                if (event.getActionCommand().equals("ApproveSelection")) {
                    File selectedFile = fc.getSelectedFile();

                }
            }
        });
        int status = fc.showOpenDialog(panel);
        if (status == JFileChooser.APPROVE_OPTION) {
            File selectedFile = fc.getSelectedFile();
            installAPKPathTextField.setText(selectedFile.getAbsolutePath());
        } else if (status == JFileChooser.CANCEL_OPTION) {


        }                                           */
    }

    private void performInstallApplication() {
        if (installAPKPathTextField.getText() != null && installAPKPathTextField.getText().length() > 0) {

            if (device != null && device.isOnline()) {

                Task.Modal task = new Task.Modal(Constants.PROJECT,"installing APK file", false ){
                    @Override
                    public void run(@NotNull ProgressIndicator progressIndicator) {

                        progressIndicator.setIndeterminate(true);
                        try {
                            String message = device.installPackage(installAPKPathTextField.getText(), true, new String[]{});
                            installedApplicationsData.clear();
                            uninstallButtons.clear();
                            loadApps();
                        } catch (InstallException e) {
                            e.printStackTrace();
                        }
                    }
                };
               ProgressManager.getInstance().run(task);
            }

        }
    }

    private void reloadInstalledAppsPanel() {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                installedAppsTableModel.fireTableDataChanged();
                if (installedApplicationPanel != null) {
                    installedApplicationPanel.revalidate();
                    installedApplicationPanel.repaint();
                }
            }
        });
    }

    private void loadApps() {
        if (device != null) {
            new Thread() {
                @Override
                public void run() {
                    super.run();
                    try {
                        device.executeShellCommand("pm list package -3", new PackageParserShellOutupReceiver(device, DeviceInfoDialog.this));
                    } catch (TimeoutException e) {
                        e.printStackTrace();
                    } catch (AdbCommandRejectedException e) {
                        e.printStackTrace();
                    } catch (ShellCommandUnresponsiveException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }.start();
        }
    }

    private JTextField installAPKPathTextField;
    private JButton openFileChooserButton, installAPKButton;
}

