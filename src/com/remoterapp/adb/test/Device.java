package com.remoterapp.adb.test;

import com.android.ddmlib.*;
import com.android.ddmlib.log.LogReceiver;
import com.remoterapp.adb.util.Logger;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * TODO Add description for class
 *
 * @author Rob Slama
 * @version 1
 */
public class Device implements IDevice {

    private String SerialNumber;

    @Override
    public String getSerialNumber() {
        return SerialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.SerialNumber = serialNumber;
    }

    @Override
    public String getAvdName() {
        return null;
    }

    @Override
    public DeviceState getState() {
        return null;
    }

    @Override
    public Map<String, String> getProperties() {
        return null;
    }

    @Override
    public int getPropertyCount() {
        return 0;
    }

    @Override
    public String getProperty(String s) {
        return null;
    }

    @Override
    public boolean arePropertiesSet() {
        return false;
    }

    @Override
    public String getPropertySync(String name) throws TimeoutException, AdbCommandRejectedException, ShellCommandUnresponsiveException, IOException {
        return null;
    }

    @Override
    public String getPropertyCacheOrSync(String name) throws TimeoutException, AdbCommandRejectedException, ShellCommandUnresponsiveException, IOException {
        return null;
    }

    @Override
    public boolean supportsFeature(Feature feature) {
        return false;
    }

    @Override
    public String getMountPoint(String s) {
        return null;
    }

    private boolean isOnline;

    @Override
    public boolean isOnline() {
        return isOnline;
    }

    public void setOnline(boolean isOnline) {
        this.isOnline = isOnline;
    }

    @Override
    public boolean isEmulator() {
        return false;
    }

    @Override
    public boolean isOffline() {
        return false;
    }

    @Override
    public boolean isBootLoader() {
        return false;
    }

    @Override
    public boolean hasClients() {
        return false;
    }

    @Override
    public Client[] getClients() {
        return new Client[0];
    }

    @Override
    public Client getClient(String s) {
        return null;
    }

    @Override
    public SyncService getSyncService() throws TimeoutException, AdbCommandRejectedException, IOException {
        return null;
    }

    @Override
    public FileListingService getFileListingService() {
        return null;
    }

    @Override
    public RawImage getScreenshot() throws TimeoutException, AdbCommandRejectedException, IOException {
        return null;
    }

    @Override
    public void startScreenRecorder(String remoteFilePath, ScreenRecorderOptions options, IShellOutputReceiver receiver) throws TimeoutException, AdbCommandRejectedException, IOException, ShellCommandUnresponsiveException {

    }

    @Override
    public void executeShellCommand(String s, IShellOutputReceiver iShellOutputReceiver) throws TimeoutException, AdbCommandRejectedException, ShellCommandUnresponsiveException, IOException {

    }

    @Deprecated
    @Override
    public void executeShellCommand(String s, IShellOutputReceiver iShellOutputReceiver, int i) throws TimeoutException, AdbCommandRejectedException, ShellCommandUnresponsiveException, IOException {

    }

    @Override
    public void runEventLogService(LogReceiver logReceiver) throws TimeoutException, AdbCommandRejectedException, IOException {

    }

    @Override
    public void runLogService(String s, LogReceiver logReceiver) throws TimeoutException, AdbCommandRejectedException, IOException {

    }

    @Override
    public void createForward(int i, int i2) throws TimeoutException, AdbCommandRejectedException, IOException {

    }

    @Override
    public void createForward(int localPort, String remoteSocketName, DeviceUnixSocketNamespace namespace) throws TimeoutException, AdbCommandRejectedException, IOException {

    }

    @Override
    public void removeForward(int i, int i2) throws TimeoutException, AdbCommandRejectedException, IOException {

    }

    @Override
    public void removeForward(int localPort, String remoteSocketName, DeviceUnixSocketNamespace namespace) throws TimeoutException, AdbCommandRejectedException, IOException {

    }

    @Override
    public String getClientName(int i) {
        return null;
    }

    @Override
    public void pushFile(String local, String remote) throws IOException, AdbCommandRejectedException, TimeoutException, SyncException {

    }

    @Override
    public void pullFile(String remote, String local) throws IOException, AdbCommandRejectedException, TimeoutException, SyncException {

    }

    @Override
    public String installPackage(String packageFilePath, boolean reinstall, String... extraArgs) throws InstallException {
        return null;
    }

    @Override
    public String syncPackageToDevice(String s) throws TimeoutException, AdbCommandRejectedException, IOException, SyncException {
        return null;
    }

    @Override
    public String installRemotePackage(String remoteFilePath, boolean reinstall, String... extraArgs) throws InstallException {
        return null;
    }


    @Override
    public void removeRemotePackage(String s) throws InstallException {

    }

    @Override
    public String uninstallPackage(String s) throws InstallException {
        return null;
    }

    @Override
    public void reboot(String s) throws TimeoutException, AdbCommandRejectedException, IOException {

    }

    @Override
    public Integer getBatteryLevel() throws TimeoutException, AdbCommandRejectedException, IOException, ShellCommandUnresponsiveException {
        return null;
    }

    @Override
    public Integer getBatteryLevel(long freshnessMs) throws TimeoutException, AdbCommandRejectedException, IOException, ShellCommandUnresponsiveException {
        return null;
    }

    @Override
    public String getName() {
        return null;
    }

    @Override
    public void executeShellCommand(String command, IShellOutputReceiver receiver, long maxTimeToOutputResponse, TimeUnit maxTimeUnits) throws TimeoutException, AdbCommandRejectedException, ShellCommandUnresponsiveException, IOException {

    }

    public static void printProperties(IDevice device) {
        if (device != null && device.arePropertiesSet()) {
            Iterator<Map.Entry<String, String>> entryIterator = (Iterator<Map.Entry<String, String>>) device.getProperties().entrySet().iterator();
            while (entryIterator.hasNext()) {
                Map.Entry entry = entryIterator.next();
                Logger.d(Device.class, "key[" + entry.getKey() + "] value[" + entry.getValue() + "]");
            }
        }
    }
}
