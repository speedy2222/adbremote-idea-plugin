package com.remoterapp.adb.test;

import com.android.ddmlib.IDevice;
import com.remoterapp.adb.persistance.Macro;

import java.util.ArrayList;
import java.util.UUID;

/**
 * TODO Add description for class
 *
 * @author Rob Slama
 * @version 1
 */
public class TestDataUtil {

    public static IDevice getDevice(){
        String serial = UUID.randomUUID().toString();
        Device device = new Device();
        device.setOnline(true);
        device.setSerialNumber(serial);
        return (IDevice)device;
    }

    public static ArrayList<Macro> getMacroTestData(){
        ArrayList<Macro> macros = new ArrayList<Macro>();
        Macro macro1 = new Macro();
        macro1.setName("macro1");
        macro1.setInputTextMacro(true);
        macros.add(macro1);

        Macro macro2 = new Macro();
        macro2.setName("macro2");
        macro2.setInputTextMacro(true);
        macros.add(macro2);

        Macro macro3 = new Macro();
        macro3.setName("macro3");
        macro3.setInputTextMacro(true);
        macros.add(macro3);

        Macro macro4 = new Macro();
        macro4.setName("macro4");
        macro4.setInputTextMacro(true);
        macros.add(macro4);

        Macro macro5 = new Macro();
        macro5.setName("macro5");
        macro5.setInputTextMacro(true);
        macros.add(macro5);

        return macros;
    }
}
