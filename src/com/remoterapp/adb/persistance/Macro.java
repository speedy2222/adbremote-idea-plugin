package com.remoterapp.adb.persistance;


import org.json.JSONException;
import org.json.JSONObject;

import java.util.UUID;

/**
 * TODO Add description for class
 *
 * @author Rob Slama
 * @version 1
 */
public class Macro {

    private String name;
    private String content;
    private String id;
    private boolean isInputTextMacro;
    private boolean isShellCommandMacro;

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(Constants.NAME+":");
        stringBuilder.append(name);
        stringBuilder.append("\r\n\t");
        stringBuilder.append(Constants.ID+":");
        stringBuilder.append(id);
        stringBuilder.append("\r\n\t");
        stringBuilder.append(Constants.CONTENT+":");
        stringBuilder.append(content);
        stringBuilder.append("\r\n\t");
        stringBuilder.append(Constants.IS_INPUT_TEXT_TYPE+":");
        stringBuilder.append(isInputTextMacro);
        stringBuilder.append("\r\n\t");
        stringBuilder.append(Constants.IS_SHELL_COMMAND_TYPE+":");
        stringBuilder.append(isShellCommandMacro);
        return stringBuilder.toString();
    }

    public interface Constants{
        public static final String ID = "id";
        public static final String CONTENT = "content";
        public static final String NAME = "name";
        public static final String IS_INPUT_TEXT_TYPE = "is_input_text_type";
        public static final String IS_SHELL_COMMAND_TYPE ="is_shell_command_type";
    }

    public JSONObject toJSON(){
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constants.ID, id);
            jsonObject.put(Constants.NAME, name);
            jsonObject.put(Constants.CONTENT, content);
            jsonObject.put(Constants.IS_INPUT_TEXT_TYPE, isInputTextMacro);
            jsonObject.put(Constants.IS_SHELL_COMMAND_TYPE, isShellCommandMacro);

        } catch (JSONException e) {
            e.printStackTrace();
            jsonObject = null;
        }
        return jsonObject;
    }

    public static Macro fromJSON(JSONObject jsonObject){
        if(jsonObject==null){
            return null;
        }
        Macro macro = new Macro();
        try {
            macro.id = jsonObject.getString(Constants.ID);
            macro.name = jsonObject.getString(Constants.NAME);
            macro.content = jsonObject.getString(Constants.CONTENT);
            macro.isInputTextMacro = jsonObject.getBoolean(Constants.IS_INPUT_TEXT_TYPE);
            macro.isShellCommandMacro = jsonObject.getBoolean(Constants.IS_SHELL_COMMAND_TYPE);
        } catch (JSONException e) {
            e.printStackTrace();
            macro = null;
        }
        return macro;
    }

    public Macro(){
        id = UUID.randomUUID().toString();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isInputTextMacro() {
        return isInputTextMacro;
    }

    public void setInputTextMacro(boolean isInputTextMacro) {
        this.isInputTextMacro = isInputTextMacro;
    }

    public boolean isShellCommandMacro() {
        return isShellCommandMacro;
    }

    public void setShellCommandMacro(boolean isShellCommandMacro) {
        this.isShellCommandMacro = isShellCommandMacro;
    }


}
