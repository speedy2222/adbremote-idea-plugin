package com.remoterapp.adb.persistance;

import com.intellij.ide.util.PropertiesComponent;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.remoterapp.adb.util.Constants;
import com.remoterapp.adb.util.Util;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * TODO Add description for class
 *
 * @author Rob Slama
 * @version 1
 */

public class PreferenceBean  implements Serializable{

    private String ADB_PATH;
    private final HashMap<String, Macro> macroHashMap = new HashMap<String, Macro>();


    public void setAdbPath(String adbPath){
        this.ADB_PATH = adbPath;
    }

    public String getAdbPath(){
        return this.ADB_PATH;
    }

    public void addMacro(Macro macro){
        if(macro!=null){
            macroHashMap.put(macro.getId(), macro);
        }
    }

    public void removeMacro(Macro macro){
        if(macro!=null){
            macroHashMap.remove(macro.getId());
        }
    }

    public Macro getMacroById(String macroID){
        return macroHashMap.get(macroID);
    }

    public HashMap<String, Macro> getMacrosMap(){
        return this.macroHashMap;
    }

    public void save(){
        PropertiesComponent propertiesComponent = PropertiesComponent.getInstance();
        propertiesComponent.setValue(Constants.ADB_PATH, ADB_PATH);
        propertiesComponent.setValue(Constants.MACROS_LIST, macrosToJSONArray());
        this.loadState();
    }

    public void loadState() {
        PropertiesComponent propertiesComponent = PropertiesComponent.getInstance();
        ADB_PATH = propertiesComponent.getValue(Constants.ADB_PATH);
        macrosFromJSON(propertiesComponent.getValue(Constants.MACROS_LIST));

    }

    public void resetProperties(){
        PropertiesComponent propertiesComponent = PropertiesComponent.getInstance();
        propertiesComponent.setValue(Constants.ADB_PATH, null);
        propertiesComponent.setValue(Constants.MACROS_LIST, null);
        this.loadState();

    }

    private String macrosToJSONArray(){
        JSONArray jsonArray = new JSONArray();
        Iterator<Map.Entry<String, Macro>> iterator = this.macroHashMap.entrySet().iterator();
        while(iterator.hasNext()){
            Map.Entry<String, Macro> entry = iterator.next();
            if(entry.getValue()!=null){
                Macro macro = entry.getValue();
                JSONObject jsonObject = macro.toJSON();
                if(jsonObject!=null){
                    jsonArray.put(jsonObject);
                }
            }
        }

        return jsonArray.toString();
    }

    private void macrosFromJSON(String content){
        if(Util.isEmpty(content)){
            return;
        }

        try {
            JSONArray jsonArray = new JSONArray(content);
            if(jsonArray!=null && jsonArray.length()>0){
                final int length = jsonArray.length();
                for(int a = 0; a < length; ++a){
                    JSONObject jsonObject = jsonArray.getJSONObject(a);
                    Macro macro = Macro.fromJSON(jsonObject);
                    if(macro!=null){
                        macroHashMap.put(macro.getId(), macro);
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
