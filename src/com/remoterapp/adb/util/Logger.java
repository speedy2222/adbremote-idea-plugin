package com.remoterapp.adb.util;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * TODO Add description for class
 *
 * @author Rob Slama
 * @version 1
 */
public class Logger {

    private static final SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat("HH:mm:ss");

    public static void d(Class clazz, String message){
        if(clazz==null){
            clazz = new Object().getClass();
        }
        if(Util.isEmpty(message)){
            message = "empty message";
        }
        System.out.println(SIMPLE_DATE_FORMAT.format(new Date())+"Logger:  ["+clazz.getSimpleName()+"]"+message);
    }

    public static void d(Object object, String message){
        if(object==null){
            object = new Object();
        }
        if(Util.isEmpty(message)){
            message = "empty message";
        }
        d(object.getClass(), message);

    }

    public static void d(Object object, long l){
        if(object==null){
            object = new Object();
        }

        d(object.getClass(), l);

    }

    public static void d(Class clazz, long l){
        if(clazz==null){
            clazz = new Object().getClass();
        }

        d(clazz.getClass(), String.valueOf(l));

    }


    //================ ERRORS ==================
    public static void e(Class clazz, String message){
        if(clazz==null){
            clazz = new Object().getClass();
        }
        if(Util.isEmpty(message)){
            message = "empty message";
        }
        d(clazz, "[ERROR]/"+message);

    }

    public static void e(Object object, String message){
        if(object==null){
            object = new Object().getClass();
        }
        if(Util.isEmpty(message)){
            message = "empty message";
        }
        d(object, "[ERROR]/"+message);

    }

}
