package com.remoterapp.adb.util;

import com.intellij.openapi.components.ApplicationComponent;
import com.intellij.openapi.fileEditor.*;
import com.intellij.openapi.fileTypes.StdFileTypes;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import org.jdom.Element;
import org.jetbrains.annotations.NotNull;

/**
 * TODO Add description for class
 *
 * @author Rob Slama
 * @version 1
 */
public class ShellOutputEditorProvider implements ApplicationComponent,FileEditorProvider {
    @Override
    public void initComponent() {

    }

    @Override
    public void disposeComponent() {

    }

    @Override
    public boolean accept(@NotNull Project project, @NotNull VirtualFile virtualFile) {

        return virtualFile.getFileType() == StdFileTypes.PLAIN_TEXT;
    }

    @NotNull
    @Override
    public FileEditor createEditor(@NotNull Project project, @NotNull VirtualFile virtualFile) {
        return new ShellOutputEditor(project, virtualFile);
    }

    @Override
    public void disposeEditor(@NotNull FileEditor fileEditor) {

    }

    @NotNull
    @Override
    public FileEditorState readState(@NotNull Element element, @NotNull Project project, @NotNull VirtualFile virtualFile) {
        return DummyFileEditorState.DUMMY;
    }

    @Override
    public void writeState(@NotNull FileEditorState fileEditorState, @NotNull Project project, @NotNull Element element) {

    }

    @NotNull
    @Override
    public String getEditorTypeId() {
        return getComponentName();
    }

    @NotNull
    @Override
    public FileEditorPolicy getPolicy() {
        return FileEditorPolicy.NONE;
    }

    @NotNull
    @Override
    public String getComponentName() {
        return "ShellOutupEditorProvider";
    }

    public static class DummyFileEditorState implements FileEditorState
    {
        public static final FileEditorState DUMMY = new DummyFileEditorState();

        public boolean canBeMergedWith(FileEditorState otherState,
                                       FileEditorStateLevel level)
        {
            return false;
        }
    }
}
