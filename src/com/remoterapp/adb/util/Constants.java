package com.remoterapp.adb.util;

import com.android.ddmlib.IDevice;
import com.intellij.openapi.project.Project;
import com.remoterapp.adb.persistance.PreferenceBean;

import java.awt.*;
import java.util.HashMap;

/**
 * TODO Add description for class
 *
 * @author Rob Slama
 * @version 1
 */
public class Constants {

    public static final String ADB_PATH = "ADB_PATH";
    public static final String MACROS_LIST ="MACROS_LIST";

    public static final String IMAGE_FOLDER_PATH = "/com/remoterapp/adb/images/";

    public static final PreferenceBean PREFERENCE_BEAN = new PreferenceBean();

    public static final HashMap<String, IDevice> CONNECTED_DEVICES = new HashMap<String, IDevice>();
    public static final HashMap<String, Boolean> DEVICE_STATE = new HashMap<String, Boolean>();

    public static Project PROJECT;

    public static final Font BOLD_12_FONT = new java.awt.Font("Lucida Grande", Font.BOLD, 12);

    static{
        PREFERENCE_BEAN.loadState();
    }
}
