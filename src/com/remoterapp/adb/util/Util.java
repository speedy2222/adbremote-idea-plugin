package com.remoterapp.adb.util;

import com.android.ddmlib.FileListingService;
import com.android.ddmlib.IDevice;
import com.intellij.openapi.actionSystem.*;
import com.intellij.openapi.util.io.FileUtil;
import com.remoterapp.adb.exception.NotSetEnvironment;
import com.remoterapp.adb.filecommander.DeviceFileSystem;
import com.remoterapp.adb.filecommander.FileSystem;
import com.remoterapp.adb.filecommander.LocalFileSystem;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Array;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * TODO Add description for class
 *
 * @author Rob Slama
 * @version 1
 */
public class Util {

    public static IDevice findDeviceBySerial(HashMap<String, IDevice> deviceHashMap, String serial) {
        if (deviceHashMap == null || deviceHashMap.size() == 0 || serial == null || serial.length() == 0) {
            return null;
        }

        return deviceHashMap.get(serial);
    }

    public static String formatText(String value) {
        value = value.replace(" ", "%s");
        return value;
    }

    public static String checkAndroidHomePath() {
        return System.getenv("ANDROID_HOME");
    }

    public static boolean isEmpty(String text) {
        if (text != null && text.trim().length() > 0) {
            return false;
        }
        return true;
    }

    public static void setSizes(JComponent component) {
        component.setMinimumSize(new Dimension(200, 100));
        component.setMaximumSize(new Dimension(400, 500));
        component.setPreferredSize(new Dimension(300, 200));
    }

    public static DefaultActionGroup mainPanelCreateToolbarActions() {
        final ActionManager actionManager = ActionManager.getInstance();
        final DefaultActionGroup group = new DefaultActionGroup();


        group.add(actionManager.getAction("ADBRemote.OpenFileCommander"));


        return group;
    }

    public static String deviceHash(IDevice iDevice) {
        if (iDevice == null) {
            return null;
        }

        if (iDevice.getProperties() != null && iDevice.getProperties().size() > 0) {
            StringBuilder stringBuilder = new StringBuilder();
            for (Map.Entry<String, String> entry : iDevice.getProperties().entrySet()) {
                stringBuilder.append(entry.getKey());
                stringBuilder.append(entry.getValue());
            }
            MessageDigest m = null;
            try {
                m = MessageDigest.getInstance("MD5");
                m.reset();
                m.update(stringBuilder.toString().getBytes());
                byte[] digest = m.digest();
                BigInteger bigInt = new BigInteger(1, digest);
                String hashtext = bigInt.toString(16);
                return hashtext;
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }


        }
        return null;
    }

    public static void deviceList(String path, IDevice iDevice) {
        if (iDevice == null) {
            return;
        }
        if (Util.isEmpty(path)) {
            path = "/";
        }

        FileListingService service = iDevice.getFileListingService();
        FileListingService.FileEntry entry = service.getRoot().findChild(path);

        FileListingService.FileEntry[] entries = service.getChildren(service.getRoot(), false, new FileListingService.IListingReceiver() {
            @Override
            public void setChildren(FileListingService.FileEntry entry, FileListingService.FileEntry[] children) {
                System.out.println();
            }

            @Override
            public void refreshEntry(FileListingService.FileEntry entry) {
                System.out.println();
            }
        });
        System.out.println();

    }

    public static String humanReadableByteCount(String bytesInString, boolean si) {
        long l = 0;
        try {
            l = Long.parseLong(bytesInString);
        } catch (Exception e) {

        }
        return humanReadableByteCount(l, si);
    }


    public static String humanReadableByteCount(long bytes, boolean si) {
        int unit = si ? 1000 : 1024;
        if (bytes < unit) return bytes + " B";
        int exp = (int) (Math.log(bytes) / Math.log(unit));
        String pre = (si ? "kMGTPE" : "KMGTPE").charAt(exp - 1) + (si ? "" : "i");
        return String.format("%.1f %sB", bytes / Math.pow(unit, exp), pre);
    }

    private static final String CACHE_DIR = "cache1234434519211291adbcommander";

    public static File getTemporaryDirectory() {
        try {
            String pathToTempDirectory = FileUtil.getTempDirectory();
            if (!Util.isEmpty(pathToTempDirectory)) {
                File cache = new File(pathToTempDirectory, CACHE_DIR);
                if (cache != null && cache.exists()) {
                    return cache;
                } else {
                    cache.mkdir();
                    return cache;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return null;
    }

    public static String getFileEntryHash(FileListingService.FileEntry fileEntry) {
        String consolidateString = fileEntry.getName() + fileEntry.getType() + fileEntry.getDate() + fileEntry.getTime() + fileEntry.getSize() + fileEntry.getInfo();
        MessageDigest m = null;
        try {
            m = MessageDigest.getInstance("MD5");
            m.reset();
            m.update(consolidateString.getBytes());
            byte[] digest = m.digest();
            BigInteger bigInt = new BigInteger(1, digest);
            String hashtext = bigInt.toString(16);
            return hashtext;
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }


    public static String getFileExtension(String name) {
        if (Util.isEmpty(name)) {
            return null;
        }

        int dotIndex = name.lastIndexOf(".");
        if (dotIndex > 0 && dotIndex < name.length() - 1) {
            return name.substring(dotIndex + 1);
        }
        return null;
    }


    public static ArrayList<FileSystem> getAvalFS() {
        final ArrayList<FileSystem> fileSystems = new ArrayList<FileSystem>();
        try {
            IDevice iDevices[] = AdbConnector.getInstance().getBridge().getDevices();
            if (iDevices != null) {
                for (IDevice device : iDevices) {
                    DeviceFileSystem deviceFileSystem = new DeviceFileSystem(device);
                    fileSystems.add(deviceFileSystem);

                }
            }
            //fileSystems.add(new LocalFileSystem());
        } catch (NotSetEnvironment notSetEnvironment) {
            //notSetEnvironment.printStackTrace();
        }
        return fileSystems;
    }

    public static String getFileNameFromFile(File file, boolean attExtension) {
        if (file==null || file.isDirectory()) {
            return null;
        }
        String absolutePath = file.getAbsolutePath();
        String fileSeparator = System.getProperty("file.separator");

        int indexLastSlash = absolutePath.lastIndexOf(fileSeparator);
        int indexLastDot = absolutePath.lastIndexOf('.');
        if (indexLastSlash > 0) {
            if(attExtension){
                return absolutePath.substring(indexLastSlash + 1);
            }else{
                if(indexLastDot>0){
                    return absolutePath.substring(indexLastSlash + 1, indexLastDot);
                }else{
                    return absolutePath.substring(indexLastSlash + 1);
                }
            }

        }
        return null;
    }



    public static void main(String[] args) {

        Logger.d(Util.class, Util.getFileNameFromFile(new File("/Users/robertslama/Pictures/2013-07-21/IMG_0473.CR2"), false));
        //Logger.d(Util.class, Util.getTemporaryDirectory().getAbsolutePath());
    }

}
