package com.remoterapp.adb.util;

import com.android.ddmlib.AndroidDebugBridge;
import com.android.ddmlib.Client;
import com.android.ddmlib.IDevice;
import com.remoterapp.adb.PluginStartPoint;
import com.remoterapp.adb.exception.NotSetEnvironment;
import com.remoterapp.adb.persistance.PreferenceBean;

import javax.swing.*;
import java.util.ArrayList;

/**
 * TODO Add description for class
 *
 * @author Rob Slama
 * @version 1
 */
public class AdbConnector implements AndroidDebugBridge.IClientChangeListener, AndroidDebugBridge.IDebugBridgeChangeListener, AndroidDebugBridge.IDeviceChangeListener {

    private static AdbConnector instance;
    private AndroidDebugBridge bridge;
    private static boolean initialized;

    private AdbConnector() throws NotSetEnvironment {
        init();
        initialized = true;
    }

    private void init() throws NotSetEnvironment {
        if(Constants.PREFERENCE_BEAN==null){
            throw new NullPointerException("PREFERENCE_BEAN is null");

        }
        Constants.PREFERENCE_BEAN.loadState();
        if (Util.isEmpty(Constants.PREFERENCE_BEAN.getAdbPath())) {
            throw new NotSetEnvironment();
        }
        AndroidDebugBridge.init(false);
        bridge = AndroidDebugBridge.createBridge(Constants.PREFERENCE_BEAN.getAdbPath(), false);
        AndroidDebugBridge.addDebugBridgeChangeListener(this);
        AndroidDebugBridge.addDeviceChangeListener(this);
        AndroidDebugBridge.addClientChangeListener(this);

    }

    public static synchronized AdbConnector getInstance() throws NotSetEnvironment {
        if (instance == null) {
            instance = new AdbConnector();
        }
        return instance;
    }

    public  AndroidDebugBridge getBridge(){
        return instance.bridge;
    }

    public boolean isInitialized(){
        return instance.initialized;
    }

    /**
     * Sent when an existing client information changed.
     * <p/>
     * This is sent from a non UI thread.
     *
     * @param client     the updated client.
     * @param changeMask the bit mask describing the changed properties. It can contain
     *                   any of the following values: {@link Client#CHANGE_INFO},
     *                   {@link Client#CHANGE_THREAD_MODE},
     *                   {@link Client#CHANGE_THREAD_DATA}, {@link Client#CHANGE_HEAP_MODE},
     *                   {@link Client#CHANGE_HEAP_DATA}, {@link Client#CHANGE_NATIVE_HEAP_DATA}
     */
    @Override
    public void clientChanged(final Client client, final int changeMask) {
        for (final CallBack callBack : callBacks) {
            if (callBack != null) {
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        callBack.clientChanged(client, changeMask);
                    }
                });

            }
        }

    }

    /**
     * Sent when a new {@link AndroidDebugBridge} is connected.
     * <p/>
     * This is sent from a non UI thread.
     *
     * @param androidDebugBridge the new {@link AndroidDebugBridge} object.
     */
    @Override
    public void bridgeChanged(final AndroidDebugBridge androidDebugBridge) {
        for (final CallBack callBack : callBacks) {
            if (callBack != null) {
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        callBack.bridgeChanged(androidDebugBridge);
                    }
                });

            }
        }

    }

    /**
     * Sent when the a device is connected to the {@link AndroidDebugBridge}.
     * <p/>
     * This is sent from a non UI thread.
     *
     * @param iDevice the new device.
     */
    @Override
    public void deviceConnected(final IDevice iDevice) {
        for (final CallBack callBack : callBacks) {
            if (callBack != null) {
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        callBack.deviceConnected(iDevice);
                    }
                });


            }
        }

        for (final DeviceChangesCallBack deviceChangesCallBack : deviceChangesCallBacks) {
            if (deviceChangesCallBack != null) {
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        deviceChangesCallBack.deviceConnected(iDevice);
                    }
                });

            }
        }

    }

    /**
     * Sent when the a device is disconnected from the {@link AndroidDebugBridge}.
     * <p/>
     * This is sent from a non UI thread.
     *
     * @param iDevice the new device.
     */
    @Override
    public void deviceDisconnected(final IDevice iDevice) {
        for (final CallBack callBack : callBacks) {
            if (callBack != null) {
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        callBack.deviceDisconnected(iDevice);
                    }
                });

            }
        }

        for (final DeviceChangesCallBack deviceChangesCallBack : deviceChangesCallBacks) {
            if (deviceChangesCallBack != null) {
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        deviceChangesCallBack.deviceDisconnected(iDevice);
                    }
                });

            }
        }

    }

    /**
     * Sent when a device data changed, or when clients are started/terminated on the device.
     * <p/>
     * This is sent from a non UI thread.
     *
     * @param iDevice    the device that was updated.
     * @param changeMask the mask describing what changed. It can contain any of the following
     *                   values: {@link IDevice#CHANGE_BUILD_INFO}, {@link IDevice#CHANGE_STATE},
     *                   {@link IDevice#CHANGE_CLIENT_LIST}
     */
    @Override
    public void deviceChanged(final IDevice iDevice, final int changeMask) {
        for (final CallBack callBack : callBacks) {
            if (callBack != null) {
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        callBack.deviceChanged(iDevice, changeMask);
                    }
                });

            }
        }

        for (final DeviceChangesCallBack deviceChangesCallBack : deviceChangesCallBacks) {
            if (deviceChangesCallBack != null) {
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        deviceChangesCallBack.deviceChanged(iDevice, changeMask);
                    }
                });

            }
        }

    }

    private static final ArrayList<CallBack> callBacks = new ArrayList<CallBack>();

    public void addCallBackListener(CallBack callBackListener) {
        if (callBackListener != null) {
            callBacks.add(callBackListener);
        }
    }
    public boolean removeCallBack(CallBack callBack){
        if(callBack!=null){
            return callBacks.remove(callBack);
        }
        return false;
    }

    private static final ArrayList<DeviceChangesCallBack> deviceChangesCallBacks = new ArrayList<DeviceChangesCallBack>();

    public void addDeviceChangesCallBackListener(DeviceChangesCallBack deviceChangesCallBack) {
        if (deviceChangesCallBack != null) {
            deviceChangesCallBacks.add(deviceChangesCallBack);
        }
    }

    public boolean removeDeviceChangesCallBack(DeviceChangesCallBack deviceChangesCallBack){
        if(deviceChangesCallBack!=null){
            return deviceChangesCallBacks.remove(deviceChangesCallBack);
        }
        return false;
    }


    public static interface CallBack extends DeviceChangesCallBack{

        public void bridgeChanged(AndroidDebugBridge androidDebugBridge);

        public void clientChanged(Client client, int changeMask);
    }

    public static interface DeviceChangesCallBack{
        public void deviceChanged(IDevice iDevice, int changeMask);

        public void deviceDisconnected(IDevice iDevice);

        public void deviceConnected(IDevice iDevice);

    }
}
