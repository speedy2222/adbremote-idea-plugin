package com.remoterapp.adb.util;

import com.android.ddmlib.IDevice;
import com.android.ddmlib.IShellOutputReceiver;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.editor.Document;
import com.intellij.openapi.fileEditor.FileDocumentManager;
import com.intellij.openapi.fileEditor.OpenFileDescriptor;
import com.intellij.openapi.fileTypes.StdFileTypes;
import com.intellij.testFramework.LightVirtualFile;

import javax.swing.*;
import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * TODO Add description for class
 *
 * @author Rob Slama
 * @version 1
 */
public class ShellOutputReceiverImpl implements IShellOutputReceiver {

    private IDevice device;

    private static final String lineSeparator = "\n";//System.getProperty("line.separator");
    private String command;
    private boolean firstRun = true;
    private static final String line = lineSeparator + "=======================================================================================";


    private LightVirtualFile virtualFile;
    private  Document document;
    private static final SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat("HH:mm:ss");

    public ShellOutputReceiverImpl(IDevice device, String command) {
        this.device = device;
        this.command = command;
        if(device==null){
            virtualFile = new LightVirtualFile(SIMPLE_DATE_FORMAT.format(new Date()) + ".output", StdFileTypes.PLAIN_TEXT, "");
        }else{
            String deviceName = null;
            if(device.isEmulator()){
                deviceName = device.getAvdName();
            }else{
                deviceName = device.getName();
            }
            virtualFile = new LightVirtualFile(deviceName+"("+SIMPLE_DATE_FORMAT.format(new Date())  + ").output", StdFileTypes.PLAIN_TEXT, "");
        }

        ApplicationManager.getApplication().runReadAction(new Runnable() {

            @Override
            public void run() {
                document = FileDocumentManager.getInstance().getDocument(virtualFile);
            }
        });
    }

    @Override
    public void addOutput(byte[] bytes, int i, int i2) {
        String message = new String(bytes, i, i2);
        Logger.d(this, this.toString() + ":bytes.length:" + bytes.length + " start:" + i + " length:" + i2);
        if(!Util.isEmpty(message) && message.toLowerCase().startsWith("error")){
            Logger.e(this, message);
        }
        if (this.device != null && this.device.isOnline()) {
            if (firstRun) {
                firstRun = false;
                message = line + lineSeparator +
                        "deviceInfo: " + deviceInfo() + lineSeparator +
                        "command: " + command + lineSeparator +
                        line + lineSeparator +
                        message;
            }
            openEditor(message);
        }

    }

    @Override
    public void flush() {

    }

    @Override
    public boolean isCancelled() {
        return false;
    }

    boolean addNewLine = true;

    private void openEditor(final String message) {
        ApplicationManager.getApplication().executeOnPooledThread(new Runnable() {
            public void run() {
                ApplicationManager.getApplication().runReadAction(new Runnable() {
                    public void run() {
                        //PsiFileFactory factory = PsiFileFactory.getInstance(PluginStartPoint.PROJECT);


                        SwingUtilities.invokeLater(new Runnable() {
                            @Override
                            public void run() {

                                if (virtualFile != null && document != null) {


                                    OpenFileDescriptor openFileDescriptor = new OpenFileDescriptor(Constants.PROJECT, virtualFile, 0);
                                    //final List<FileEditor> editor = FileEditorManager.getInstance(Constants.PROJECT).openEditor(openFileDescriptor, true);
                                    ShellOutputEditor editor = (ShellOutputEditor) new ShellOutputEditorProvider().createEditor(Constants.PROJECT, virtualFile);

                                    ApplicationManager.getApplication().runWriteAction(new Runnable() {
                                        @Override
                                        public void run() {
                                            String _message;
                                            if (addNewLine) {
                                                _message = lineSeparator + message.replaceAll("\r\n", lineSeparator);
                                                document.setText(document.getText() + _message);
                                            } else {
                                                _message = message.replaceAll("\\r?\\n", lineSeparator);
                                                document.setText(document.getText() + _message);

                                            }


                                        }
                                    });

                                }
                            }

                        });

                    }
                });
            }
        });


    }

    private String deviceInfo() {
        String info = null;
        if (this.device != null) {
            info = this.device.getProperty("ro.product.manufacturer")
                    + " " + this.device.getProperty("ro.product.model")

                    + "(" + this.device.getProperty("ro.build.version.release") + ") serialNo:"

                    + this.device.getSerialNumber();
        }
        return info;
    }


}
