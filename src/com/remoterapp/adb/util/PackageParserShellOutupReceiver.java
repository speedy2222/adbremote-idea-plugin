package com.remoterapp.adb.util;

import com.android.ddmlib.IDevice;
import com.android.ddmlib.IShellOutputReceiver;
import com.remoterapp.adb.ui.dialogs.DeviceInfoDialog;

import java.io.*;
import java.util.ArrayList;

/**
 * TODO Add description for class
 *
 * @author Rob Slama
 * @version 1
 */
public class PackageParserShellOutupReceiver implements IShellOutputReceiver {

    private IDevice device;

    private DeviceInfoDialog deviceInfoDialog;

    public PackageParserShellOutupReceiver(IDevice iDevice, DeviceInfoDialog deviceInfoDialog) {
        this.device = iDevice;
        this.deviceInfoDialog = deviceInfoDialog;
    }

    @Override
    public void addOutput(byte[] data, int offset, int length) {
        final String message = new String(data, offset, length);
        new Thread(){
            @Override
            public void run() {
                super.run();
                parsePackages(message);
            }
        }.start();


    }


    @Override
    public void flush() {

    }

    @Override
    public boolean isCancelled() {
        return false;
    }


    private void parsePackages(String message) {
        //Logger.d(this, message);
        InputStream is = new ByteArrayInputStream(message.getBytes());
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        String line;
        final ArrayList<String> packages = new ArrayList<String>();
        int offset = "package:".length();
        try {
            while ((line = br.readLine()) != null) {
                if(offset<line.length()){
                    line = line.substring(offset).trim();
                    if (!Util.isEmpty(line)) {
                        packages.add(line);
                    }
                }

            }
            if(deviceInfoDialog!=null){
                new Thread(){
                    @Override
                    public void run() {
                        super.run();
                        try {
                            sleep(2000);
                            deviceInfoDialog.packageUpdate(packages);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }.start();

            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}

