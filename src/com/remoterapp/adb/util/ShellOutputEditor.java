package com.remoterapp.adb.util;

import com.intellij.codeHighlighting.BackgroundEditorHighlighter;
import com.intellij.ide.structureView.StructureViewBuilder;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.editor.Document;
import com.intellij.openapi.fileEditor.*;
import com.intellij.openapi.fileTypes.FileType;
import com.intellij.openapi.fileTypes.FileTypeConsumer;
import com.intellij.openapi.fileTypes.FileTypeFactory;
import com.intellij.openapi.fileTypes.FileTypes;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.util.Key;
import com.intellij.openapi.vfs.VirtualFile;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import java.awt.*;
import java.beans.PropertyChangeListener;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * TODO Add description for class
 *
 * @author Rob Slama
 * @version 1
 */
public class ShellOutputEditor implements FileEditor,
        FileEditorManagerListener {

    private final VirtualFile file;
    private final Project project;
    private JPanel mainPanel = new JPanel(new BorderLayout());

    @SuppressWarnings("deprecation")
    public ShellOutputEditor(final Project project, final VirtualFile virtualFile){
        this.file = virtualFile;
        this.project = project;

        FileEditorManager.getInstance(project).addFileEditorManagerListener(this);
        //mainPanel.setLayout(new BorderLayout());
        //mainPanel.add(new Label("XXXXX"), BorderLayout.CENTER);

        ApplicationManager.getApplication().invokeLater( new Runnable()
        {
            public void run()
            {
                FileEditorManager.getInstance(project).openFile(virtualFile,true);
            }
        } );
    }

    @NotNull
    @Override
    public JComponent getComponent() {
        return mainPanel;
    }

    @Nullable
    @Override
    public JComponent getPreferredFocusedComponent() {
        return mainPanel;
    }

    @NotNull
    @Override
    public String getName() {
        return "Preview";
    }

    @NotNull
    @Override
    public FileEditorState getState(@NotNull FileEditorStateLevel fileEditorStateLevel) {
        return ShellOutputEditorProvider.DummyFileEditorState.DUMMY;
    }

    @Override
    public void setState(@NotNull FileEditorState fileEditorState) {

    }

    @Override
    public boolean isModified() {
        return false;
    }

    @Override
    public boolean isValid() {
        return true;
    }

    @Override
    public void selectNotify() {

    }

    @Override
    public void deselectNotify() {

    }

    private void reloadContent()
    {
        FileDocumentManager fileDocManager =
                FileDocumentManager.getInstance();
        Document doc = fileDocManager.getDocument(file);
        fileDocManager.saveDocument( doc );

    }

    @Override
    public void addPropertyChangeListener(@NotNull PropertyChangeListener propertyChangeListener) {

    }

    @Override
    public void removePropertyChangeListener(@NotNull PropertyChangeListener propertyChangeListener) {

    }

    @Nullable
    @Override
    public BackgroundEditorHighlighter getBackgroundHighlighter() {
        return null;
    }

    @Nullable
    @Override
    public FileEditorLocation getCurrentLocation() {
        return null;
    }

    @Nullable
    @Override
    public StructureViewBuilder getStructureViewBuilder() {
        return null;
    }

    @Override
    public void dispose() {

    }

    @Override
    public void fileOpened(@NotNull FileEditorManager fileEditorManager, @NotNull VirtualFile virtualFile) {
        System.out.println();
    }

    @Override
    public void fileClosed(@NotNull FileEditorManager fileEditorManager, @NotNull VirtualFile virtualFile) {
        System.out.println();
    }

    @Override
    public void selectionChanged(@NotNull FileEditorManagerEvent fileEditorManagerEvent) {
        if (FileEditorManager.getInstance(project).getSelectedEditor(file) == this )
            reloadContent ();
    }

    @Nullable
    @Override
    public <T> T getUserData(@NotNull Key<T> tKey) {
        return null;
    }

    @Override
    public <T> void putUserData(@NotNull Key<T> tKey, @Nullable T t) {

    }



}
