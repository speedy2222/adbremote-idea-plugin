package com.remoterapp.adb.util;

import com.android.ddmlib.*;
import com.remoterapp.adb.exception.NotSetEnvironment;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;

/**
 * TODO Add description for class
 *
 * @author Rob Slama
 * @version 1
 */
public class BatteryLevelThread extends Thread{

    private static final Object lock = new Object();
    private static BatteryLevelThread instance;

    private BatteryLevelThread(){
        instance = this;
        this.start();
    }

    public static BatteryLevelThread getInstance(){
        synchronized (lock){
            if(instance==null){
                instance = new BatteryLevelThread();
            }
            return instance;
        }

    }

    @Override
    public void run() {
        super.run();
        while(true){
            try {
                if(AdbConnector.getInstance()!=null){
                    AndroidDebugBridge bridge = AdbConnector.getInstance().getBridge();
                    if(bridge!=null){
                        IDevice[] devices = bridge.getDevices();
                        if(devices!=null && devices.length>0){
                            for(IDevice device: devices){
                                for(WeakReference<BatteryLevelUpdater> updaterWeakReference: listeners){
                                    if(updaterWeakReference.get()!=null){
                                        try {
                                            updaterWeakReference.get().batteryLevel(device, device.getBatteryLevel(1000));
                                        } catch (TimeoutException e) {
                                            e.printStackTrace();
                                        } catch (AdbCommandRejectedException e) {
                                            e.printStackTrace();
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        } catch (ShellCommandUnresponsiveException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

            } catch (NotSetEnvironment notSetEnvironment) {
                //notSetEnvironment.printStackTrace();
            }
            try {
                sleep(120000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }

    private static final ArrayList<WeakReference<BatteryLevelUpdater>> listeners = new ArrayList<WeakReference<BatteryLevelUpdater>>();

    public void addListener(BatteryLevelUpdater listener){
        if(listener!=null){
            listeners.add(new WeakReference<BatteryLevelUpdater>(listener));
        }
    }

    public interface BatteryLevelUpdater{
        public void batteryLevel(IDevice device, int level);
    }
}
