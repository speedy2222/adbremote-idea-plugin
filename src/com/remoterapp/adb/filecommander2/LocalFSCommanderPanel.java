package com.remoterapp.adb.filecommander2;

import com.intellij.icons.AllIcons;
import com.intellij.ui.components.JBScrollPane;
import com.intellij.ui.table.JBTable;
import com.remoterapp.adb.filecommander.CommanderPanel;
import com.remoterapp.adb.filecommander.FileSystem;
import com.remoterapp.adb.util.Logger;
import com.remoterapp.adb.util.Util;

import javax.swing.*;
import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.text.SimpleDateFormat;

/**
 * TODO Add description for class
 *
 * @author Rob Slama
 * @version 1
 */
public class LocalFSCommanderPanel extends CommanderPanel {

    private JPanel mainCommanderPanel;
    private static final Object lock = new Object();
    private static CommanderPanel INSTANCE;

    private MyCustomModel myModel;
    private static FileCommander parent;
    private JBTable jTable;
    private String currentPath;
    protected File currentDirectory;


    private LocalFSCommanderPanel(FileCommander parent, FileSystem fileSystem) {
        super(parent, fileSystem);
        this.parent = parent;
        currentPath = System.getProperty("user.home");
        currentDirectory = new File(currentPath);
        parent.updateRightPanelPathLabel(currentDirectory.getAbsolutePath());
        initPanel();
    }


    @Override
    public void lostFocusRequest() {
        Logger.d(this, "lostFocus");
        this.parent.requestFocusInWindow();

       // jTable.requestFocusInWindow();
    }

    @Override
    public void setFocus() {
        Logger.d(this, "setFocus");
      jTable.requestFocus();
    }

    @Override
    public JPanel getUI() {
        createMenuBar();
        jTable.repaint();
        mainCommanderPanel.revalidate();
        mainCommanderPanel.repaint();
        return mainCommanderPanel;
    }

    public void createMenuBar() {


    }


    public static CommanderPanel getInstance(FileCommander dialog, FileSystem fileSystem) {
        synchronized (lock) {
            if (INSTANCE == null) {
                INSTANCE = new LocalFSCommanderPanel(dialog, fileSystem);
            }
            ((LocalFSCommanderPanel) INSTANCE).parent = dialog;
            return INSTANCE;
        }
    }

    public void refresh() {
        jTable.revalidate();
        jTable.repaint();
    }

    private boolean tableHasFocus = false;

    private void initPanel() {
        mainCommanderPanel = new JPanel(new BorderLayout());


        jTable = new JBTable(myModel = new MyCustomModel(this));
        jTable.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent focusEvent) {
                Logger.d(this, "hasFocus");
                tableHasFocus= true;
            }

            @Override
            public void focusLost(FocusEvent focusEvent) {
                Logger.d(this, "lostFocus");
                tableHasFocus = false;
            }
        });


        jTable.setDefaultRenderer(Object.class, new TableCellRenderer(){
            private DefaultTableCellRenderer DEFAULT_RENDERER =  new DefaultTableCellRenderer();


            @Override
            public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                Component c = DEFAULT_RENDERER.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                if(isSelectedRow(row) && tableHasFocus){
                    c.setBackground(Color.BLUE);
                }else if(isSelectedRow(row) && !tableHasFocus){
                    c.setBackground(Color.LIGHT_GRAY);
                } else{
                    c.setBackground(jTable.getBackground());
                }


                return c;
            }

        });



        jTable.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        jTable.getColumnModel().getColumn(0).setCellRenderer(new CellNameRenderer());
        jTable.getColumnModel().getColumn(0).setMinWidth(150);
        jTable.getColumnModel().getColumn(1).setMinWidth(50);
        jTable.getColumnModel().getColumn(2).setMinWidth(150);

        JBScrollPane scrollPane = new JBScrollPane(jTable,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        jTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

        mainCommanderPanel.add(scrollPane, BorderLayout.CENTER);
        jTable.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent mouseEvent) {
                super.mouseClicked(mouseEvent);
                if (mouseEvent.getClickCount() == 2) {
                    int[] rows = jTable.getSelectedRows();
                    if (rows != null && rows.length == 1) {
                        File file = myModel.getValueAtRow(rows[0]);
                        if (file != null && file.isDirectory()) {
                            if (myModel.loadFolder(file)) {
                                parent.updateRightPanelPathLabel(file.getAbsolutePath());
                                jTable.revalidate();
                                jTable.repaint();
                            }
                        }
                    }
                }
            }
        });
        jTable.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent keyEvent) {

            }

            @Override
            public void keyPressed(KeyEvent keyEvent) {
                final int[] rows = jTable.getSelectedRows();

                if (rows != null && rows.length > 0) {
                    if (keyEvent.getKeyCode() == KeyEvent.VK_BACK_SPACE) {
                        if (myModel.loadUpDirectory()) {
                            parent.updateRightPanelPathLabel(currentDirectory.getAbsolutePath());
                            jTable.revalidate();
                            jTable.repaint();
                        }

                    } else if (keyEvent.getKeyCode() == KeyEvent.VK_ENTER) {

                        if (rows != null && rows.length == 1) {
                            File file = myModel.getValueAtRow(rows[0]);
                            if (file != null && file.isDirectory()) {
                                if (myModel.loadFolder(file)) {
                                    parent.updateRightPanelPathLabel(file.getAbsolutePath());
                                    jTable.revalidate();
                                    jTable.repaint();
                                }
                            }
                        }


                    } else if (keyEvent.getKeyCode() == KeyEvent.VK_F5) {


                    } else if (keyEvent.getKeyCode() == KeyEvent.VK_SPACE) {

                    }else if (keyEvent.getKeyCode() == KeyEvent.VK_TAB) {

                        parent.actionTAB(parent.currentLeftPanel);

                    }
                }
            }

            @Override
            public void keyReleased(KeyEvent keyEvent) {

            }


        });
    }




    static class MyCustomModel extends AbstractTableModel {

        private static final String COLUMN_NAME = "name";
        private static final int COLUMN_INDEX_NAME = 0;


        private static final String COLUMN_SIZE = "size";
        private static final int COLUMN_INDEX_SIZE = 1;

        private static final String COLUMN_DATE = "lastMoodified";
        private static final int COLUMN_INDEX_DATE = 2;

        private static final String[] columnsNames = new String[]{COLUMN_NAME, COLUMN_SIZE, COLUMN_DATE};
        private LocalFSCommanderPanel parent;

        public MyCustomModel(LocalFSCommanderPanel parent) {

            //super(new Object[parent.currentDirectory.listFiles().length][columnsNames.length], columnsNames);
            this.parent = parent;
        }

        @Override
        public int getRowCount() {


            return parent.currentDirectory != null ? parent.currentDirectory.listFiles().length : 0;
        }

        @Override
        public int getColumnCount() {
            return columnsNames.length;
        }

        @Override
        public String getColumnName(int index) {
            return columnsNames[index];
        }

        @Override
        public Class<?> getColumnClass(int index) {
            return getValueAt(0, index).getClass();
        }

        @Override
        public boolean isCellEditable(int row, int col) {
            return false;
        }

        @Override
        public Object getValueAt(int row, int col) {
            switch (col) {
                case COLUMN_INDEX_NAME:
                    return parent.currentDirectory.listFiles()[row];
                case COLUMN_INDEX_SIZE:
                    if (parent.currentDirectory.listFiles()[row].isDirectory()) {
                        return "";
                    } else {
                        return Util.humanReadableByteCount(parent.currentDirectory.listFiles()[row].length(), false);
                    }

                case COLUMN_INDEX_DATE:
                    return sdf.format(parent.currentDirectory.listFiles()[row].lastModified());


            }
            return null;
        }

        public File getValueAtRow(int row) {
            try {
                return parent.currentDirectory.listFiles()[row];
            } catch (Exception e) {
                return null;
            }


        }

        public boolean loadFolder(File fileEntry) {
            if (fileEntry != null && fileEntry.isDirectory()) {
                try {
                    parent.currentDirectory = fileEntry;
                    parent.currentPath = fileEntry.getAbsolutePath();


                    return true;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return false;
        }

        public boolean loadUpDirectory() {
            if (parent.currentDirectory == null || parent.currentDirectory.getParentFile() == null) {
                return false;
            }

            return loadFolder(parent.currentDirectory.getParentFile());
        }

        final SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");

        @Override
        public void setValueAt(Object o, int i, int i2) {

        }

        @Override
        public void addTableModelListener(TableModelListener tableModelListener) {

        }

        @Override
        public void removeTableModelListener(TableModelListener tableModelListener) {

        }
    }

    private class CellNameRenderer extends DefaultTableCellRenderer {
        public CellNameRenderer() {
            setOpaque(true);
        }

        @Override
        public void setValue(Object aValue) {

            if (aValue instanceof File) {
                File entry = (File) aValue;
                if (entry.isDirectory()) {
                    setIcon(AllIcons.Nodes.Folder);
                } else {
                    setIcon(AllIcons.FileTypes.Any_type);
                }
                super.setValue(entry.getName() == null ? "" : entry.getName());
            } else {
                super.setValue(aValue);
            }


        }


        @Override
        public Component getTableCellRendererComponent(JTable jTable, Object o, boolean isSelected, boolean hasFocus, int row, int col) {
            super.getTableCellRendererComponent(jTable, o, isSelected, hasFocus, row, col);

            if(isSelectedRow(row) && tableHasFocus){
                setBackground(Color.BLUE);
            }else if(isSelectedRow(row) && !tableHasFocus){
                setBackground(Color.LIGHT_GRAY);
            } else{
                setBackground(jTable.getBackground());
            }

            return this;

        }


    }

    private boolean isSelectedRow(int rowIndex){
        int [] selRows = jTable.getSelectedRows();
        if(selRows!=null && selRows.length>0){
            for(int row: selRows){
                if(row == rowIndex){
                    return true;
                }
            }
        }
        return false;
    }
}
