package com.remoterapp.adb.filecommander2;

import com.android.ddmlib.*;
import com.intellij.openapi.progress.ProgressIndicator;
import com.intellij.openapi.progress.ProgressManager;
import com.intellij.openapi.progress.Task;
import com.intellij.ui.components.panels.VerticalBox;
import com.remoterapp.adb.components.CustomCombo;
import com.remoterapp.adb.exception.NotSetEnvironment;
import com.remoterapp.adb.filecommander.*;
import com.remoterapp.adb.util.AdbConnector;
import com.remoterapp.adb.util.Constants;
import com.remoterapp.adb.util.Logger;
import com.remoterapp.adb.util.Util;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import sun.swing.SwingUtilities2;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * TODO Add description for class
 *
 * @author Rob Slama
 * @version 1
 */
public class FileCommander extends JDialog implements AdbConnector.DeviceChangesCallBack {


    private static FileCommander INSTANCE = null;
    private static final Object lock = new Object();
    private static final HashMap<String, DeviceCommanderPanel> createdDevicePanels = new HashMap<String, DeviceCommanderPanel>();
    public DeviceCommanderPanel currentLeftPanel;
    public LocalFSCommanderPanel currentRightPanel;

    private FileCommander() {
        setTitle("ADB File Manager");
        prepareUI();
        try {
            AdbConnector.getInstance().addDeviceChangesCallBackListener(this);
        } catch (NotSetEnvironment notSetEnvironment) {
            notSetEnvironment.printStackTrace();
        }
        setModal(true);
    }

    public static FileCommander getInstance() {
        synchronized (lock) {
            if (INSTANCE == null) {
                INSTANCE = new FileCommander();
            }
            return INSTANCE;
        }
    }


    private void prepareUI() {
        setPreferredSize(new Dimension(800, 600));
        setMinimumSize((new Dimension(800, 600)));
        setLocationRelativeTo(null);


        JPanel mainUIPanel = new JPanel();
        mainUIPanel.setLayout(new BorderLayout());

        //add menuPanel
        mainUIPanel.add(prepareMenu(), BorderLayout.NORTH);

        //create JSplitPanel with 2 components
        final JSplitPane jSplitPane = new JSplitPane();
        //jSplitPane.setOneTouchExpandable(true);
        jSplitPane.setResizeWeight(0.5);
        jSplitPane.setLeftComponent(prepareLeftPanel());
        jSplitPane.setRightComponent(prepareRightPanel());

        //add splitpanel to mainUI
        mainUIPanel.add(jSplitPane, BorderLayout.CENTER);


        setLayout(new BorderLayout());
        add(mainUIPanel, BorderLayout.CENTER);


    }

    private JPanel prepareMenu() {
        JPanel menuPanel = new JPanel();
        menuPanel.setBackground(Color.red);
        return menuPanel;
    }


    private final String CURRENT_PATH_TEXT = "Current path:";
    private final JLabel leftPanelCurrentPath = new JLabel(CURRENT_PATH_TEXT);
    private final CustomCombo customCombo = new CustomCombo(Util.getAvalFS());
    private final JPanel leftDevicePanel = new JPanel(new BorderLayout());

    private JPanel prepareLeftPanel() {


        Box box = Box.createVerticalBox();

        Box firstRow = Box.createHorizontalBox();
        firstRow.setAlignmentX(LEFT_ALIGNMENT);
        firstRow.add(leftPanelCurrentPath);

        box.add(firstRow);

        Box secondRow = Box.createHorizontalBox();
        secondRow.setAlignmentX(LEFT_ALIGNMENT);
        secondRow.add(customCombo);
        customCombo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                Logger.d(this, "actionPerformed...");
                CustomCombo customCombo = (CustomCombo) actionEvent.getSource();
                //check if panel is already created
                if (customCombo.getSelectedItem() instanceof FileSystem) {
                    FileSystem fileSystem = (FileSystem) customCombo.getSelectedItem();
                    DeviceCommanderPanel deviceCommanderPanel = createdDevicePanels.get(((FileSystem) customCombo.getSelectedItem()).getDevice().getSerialNumber());
                    if (deviceCommanderPanel == null) {
                        deviceCommanderPanel = new DeviceCommanderPanel(FileCommander.this, ((FileSystem) customCombo.getSelectedItem()));
                        createdDevicePanels.put(deviceCommanderPanel.getFileSystem().getDevice().getSerialNumber(), deviceCommanderPanel);

                    }
                    attachLeftPanelContent(leftDevicePanel, deviceCommanderPanel);
                    System.out.println();
                }

            }
        });

        box.add(secondRow);

        leftDevicePanel.add(box, BorderLayout.NORTH);


        return leftDevicePanel;
    }

    public void updateLeftPanelPathLabel(String path) {
        leftPanelCurrentPath.setText(CURRENT_PATH_TEXT + path);
    }

    private void attachLeftPanelContent(JPanel leftDevicePanel, DeviceCommanderPanel deviceCommanderPanel) {
        if (currentLeftPanel != null) {
            leftDevicePanel.remove(currentLeftPanel.getUI());
        }
        Logger.d(this, "Component count after attachLeftPanel " + leftDevicePanel.getComponentCount());
        updateLeftPanelPathLabel(deviceCommanderPanel.myModel.currentRoot.getFullPath());

        currentLeftPanel = deviceCommanderPanel;
        currentLeftPanel.setFocus();
        leftDevicePanel.add(currentLeftPanel.getUI(), BorderLayout.CENTER);
        leftDevicePanel.revalidate();
        leftDevicePanel.repaint();
    }


    private final JLabel rightPanelCurrentPath = new JLabel(CURRENT_PATH_TEXT);
    private final JPanel rightDevicePanel = new JPanel(new BorderLayout());

    private JComponent prepareRightPanel() {
        currentRightPanel = (LocalFSCommanderPanel) LocalFSCommanderPanel.getInstance(this, new LocalFileSystem());

        rightPanelCurrentPath.setAlignmentX(LEFT_ALIGNMENT);


        rightDevicePanel.add(rightPanelCurrentPath, BorderLayout.NORTH);
        rightDevicePanel.add(currentRightPanel.getUI(), BorderLayout.CENTER);
        return rightDevicePanel;
    }

    public void updateRightPanelPathLabel(String path) {
        rightPanelCurrentPath.setText(CURRENT_PATH_TEXT + path);
    }

    /**
     * Copy files from device to local filesystem
     *
     * @param sourceDevice
     * @param filesToCopy
     */
    public void copyFromDevice(final IDevice sourceDevice, final FileListingService.FileEntry[] filesToCopy) {


        if (sourceDevice == null
                || !sourceDevice.isOnline()
                || filesToCopy == null
                || filesToCopy.length == 0) {
            return;
        }
        if (currentRightPanel == null) {
            //TODO alert window
            return;
        }

        if (currentRightPanel.currentDirectory == null || currentRightPanel.currentDirectory.isFile()) {
            //TODO alert window
            return;
        }


        final File destinationFolder = currentRightPanel.currentDirectory;

        /*if (filesToCopy.length == 1 && !filesToCopy[0].isDirectory()) {
            //TODO open copy dialog with option to rename
            return;
        }*/

        //prepare copy files
        Task.Modal modal = new Task.Modal(Constants.PROJECT, "copying", true) {

            @Override
            public void onCancel() {
                super.onCancel();
                Logger.d(this, "onCancel");
            }

            @Override
            public void onSuccess() {
                super.onSuccess();
                Logger.d(this, "onSuccess");
            }

            @Nullable
            @Override
            public NotificationInfo notifyFinished() {
                Logger.d(this, "notifyFinished");
                return super.notifyFinished();

            }


            @Override
            public void run(@NotNull ProgressIndicator progressIndicator) {

                prepareFullList(filesToCopy);
                System.out.println();
            }

            private HashMap<String, ArrayList<FileListingService.FileEntry>> list = new HashMap<String, ArrayList<FileListingService.FileEntry>>();

            private String currentFolder = null;

            private Pair prepareFullList(FileListingService.FileEntry[] fileEntries) {
                final int length = fileEntries.length;
                for (int a = 0; a < length; ++a) {
                    FileListingService.FileEntry fileEntry = fileEntries[a];

                    if (!fileEntry.isDirectory()) {
                        Pair pair = new Pair(currentFolder, fileEntry);
                        if (pair != null) {
                            if (pair.path == null) {
                                pair.path = "DEFAULT-FOLDER";
                            }
                            if(list.get(currentFolder)==null){
                                list.put(pair.path, new ArrayList<FileListingService.FileEntry>());
                            }
                            list.get(currentFolder).add(pair.fileEntry);

                        }
                    } else if (fileEntry.isDirectory()) {
                        currentFolder = fileEntry.getFullPath();
                        try {

                            prepareFullList(sourceDevice.getFileListingService().getChildrenSync(fileEntry));
                        } catch (TimeoutException e) {
                            e.printStackTrace();
                        } catch (AdbCommandRejectedException e) {
                            e.printStackTrace();
                        } catch (ShellCommandUnresponsiveException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    }


                }


                return null;
            }

            class Pair {
                public Pair(String path, FileListingService.FileEntry fileEntry) {
                    this.fileEntry = fileEntry;
                    this.path = path;
                }

                FileListingService.FileEntry fileEntry;
                String path;
            }
        };
        ProgressManager.getInstance().run(modal);

    }

    /**
     * Copy files from local filesystem to device's folder
     *
     * @param filesToCopy
     * @param targetDevice
     * @param destinationFolder
     */
    public void copyFromFileSystem(File[] filesToCopy, IDevice targetDevice, FileListingService.FileEntry destinationFolder) {
        if (filesToCopy == null
                || filesToCopy.length == 0
                || targetDevice == null
                || !targetDevice.isOnline()
                || destinationFolder == null) {
            return;
        }
    }

    @Override
    public void deviceChanged(IDevice iDevice, int changeMask) {
        if (changeMask == IDevice.CHANGE_STATE || iDevice.isOnline()) {
            customCombo.addDevice(iDevice);
        }
    }

    @Override
    public void deviceDisconnected(IDevice iDevice) {
        //device were disconnected .... test if this device is currently selected or not
        if (currentLeftPanel != null && currentLeftPanel.getFileSystem().getDevice().getSerialNumber().equals(iDevice.getSerialNumber())) {
            leftDevicePanel.remove(currentLeftPanel.getUI());


            leftPanelCurrentPath.setText(CURRENT_PATH_TEXT);
            leftDevicePanel.revalidate();
            leftDevicePanel.repaint();
        }
        //remove device from connectedDeviceFileSystems and from customCombo
        createdDevicePanels.remove(iDevice.getSerialNumber());
        customCombo.removeDevice(iDevice);
        Logger.d(this, "Component count after deviceDisconnect " + leftDevicePanel.getComponentCount());
    }

    @Override
    public void deviceConnected(IDevice iDevice) {

    }

    public void actionTAB(CommanderPanel panel) {
        if (panel == null) {
            return;
        }
        panel.setFocus();
    }

    public void setFocus(CommanderPanel panel) {
        if (panel == null) {
            return;
        }
        panel.lostFocusRequest();
    }

}
