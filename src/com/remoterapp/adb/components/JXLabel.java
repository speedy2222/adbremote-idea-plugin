package com.remoterapp.adb.components;

import javax.swing.*;

/**
 * TODO Add description for class
 *
 * @author Rob Slama
 * @version 1
 */
public class JXLabel<T> extends JLabel {

    private T tag;

    public void setTag(T tag){
        this.tag = tag;
    }

    public T getTag(){
        return this.tag;
    }
}
