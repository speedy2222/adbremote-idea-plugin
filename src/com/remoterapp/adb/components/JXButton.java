package com.remoterapp.adb.components;

import javax.swing.*;
import java.awt.*;

/**
 * TODO Add description for class
 *
 * @author Rob Slama
 * @version 2
 */
public class JXButton<T> extends JButton {

    private T tag;
    private Icon normalIcon, pressedIcon, overIcon;

    public JXButton(Icon normalIcon, Icon pressedIcon, Icon overIcon) {
        super(normalIcon);
        this.normalIcon = normalIcon;
        this.pressedIcon = pressedIcon;
        this.overIcon = overIcon;
        setRolloverIcon(overIcon);
        setPressedIcon(pressedIcon);
        setContentAreaFilled(true);
        setBorderPainted(false);
        setFocusPainted(false);

        setOpaque(true);
        setBorder(BorderFactory.createEmptyBorder());
        //setBorderPainted(false);

    }


    public Icon getPressedIcon() {
        return pressedIcon;
    }

    public Icon getOverIcon() {
        return overIcon;
    }

    public Icon getNormalIcon() {
        return normalIcon;
    }

    public void setTag(T tag) {
        this.tag = tag;
    }

    public T getTag() {
        return tag;
    }
}
