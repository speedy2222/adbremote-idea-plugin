package com.remoterapp.adb.components;

import org.jdesktop.swingx.JXMultiSplitPane;
import org.jdesktop.swingx.MultiSplitLayout;

import javax.swing.*;
import java.awt.*;

/**
 * TODO Add description for class
 *
 * @author Rob Slama
 * @version 1
 */
public class SplitModel extends MultiSplitLayout.Split {

    public static final String P1 = "1";
    public static final String P2 = "2";
    public static final String P3 = "3";
    public static final String P4 = "4";


    public SplitModel() {
        setRowLayout(false);
        MultiSplitLayout.Leaf p1 = new MultiSplitLayout.Leaf(P1);
        MultiSplitLayout.Leaf p2 = new MultiSplitLayout.Leaf(P2);
        MultiSplitLayout.Leaf p3 = new MultiSplitLayout.Leaf(P3);
        MultiSplitLayout.Leaf p4 = new MultiSplitLayout.Leaf(P4);


        setChildren(p1, new MultiSplitLayout.Divider(), p2, new MultiSplitLayout.Divider(),
                p3, new MultiSplitLayout.Divider(), p4);



        p1.setWeight(0.3);
        p2.setWeight(0.5);
        p3.setWeight(0.1);
        p4.setWeight(0.1);

    }

    public static class BevelDividerPainter extends JXMultiSplitPane.DividerPainter {
        private JComponent owner;

        public BevelDividerPainter(JComponent c) {
            owner = c;
        }

        public void doPaint(Graphics2D g, MultiSplitLayout.Divider divider, int width, int height) {
            Color c = owner.getBackground();

            g.setColor(c);
            g.fillRect(0, 0, width, height);

            int position = 0;
            if (divider.isVertical()) {
                position = width / 2;
                g.setColor(c.brighter());
                g.drawLine(position, 0, position, 0);
                g.setColor(c.darker());
                g.drawLine(position + 1, 0, position + 1, 0);

            } else {
                position = height / 2;
                g.setColor(c.brighter());
                g.drawLine(0, position, width, position);
                g.setColor(c.darker());
                g.drawLine(0, position + 1, width, position + 1);

            }
        }
    }


}
