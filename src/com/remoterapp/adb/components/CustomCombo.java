package com.remoterapp.adb.components;

import com.android.ddmlib.IDevice;
import com.intellij.openapi.ui.ComboBox;
import com.remoterapp.adb.filecommander.DeviceFileSystem;
import com.remoterapp.adb.filecommander.FileSystem;
import com.remoterapp.adb.util.Logger;

import javax.swing.*;
import javax.swing.event.ListDataListener;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

/**
 * TODO Add description for class
 *
 * @author Rob Slama
 * @version 1
 */
public class CustomCombo extends ComboBox {

    private HashMap<String, FileSystem> fileSystems = new HashMap<String, FileSystem>();
    private ActionListener actionListener;

    public CustomCombo(ArrayList<FileSystem> content) {
        this(content, null);
    }

    public CustomCombo(ArrayList<FileSystem> content, ActionListener actionListener) {
        if (content != null) {
            for (FileSystem fileSystem : content) {
                fileSystems.put(fileSystem.getDevice().getSerialNumber(), fileSystem);
                addItem(fileSystem);
            }

        }
        //setModel(model);
        setRenderer(renderer);
        setMaximumRowCount(10);
        if (actionListener != null) {
            this.actionListener = actionListener;
            addActionListener(actionListener);
        }
        insertItemAt("Connected device(s)", 0);
        setSelectedIndex(0);
    }

    public synchronized void removeDevice(IDevice iDevice) {
        Logger.d(this, "removeDevice");
        if (iDevice == null) {
            return;
        }

        FileSystem fileSystem = fileSystems.get(iDevice.getSerialNumber());
        if (fileSystem != null) {

            if (getSelectedItem() instanceof FileSystem && ((FileSystem) getSelectedItem()).getDevice().getSerialNumber().equals(iDevice.getSerialNumber())) {
                setSelectedIndex(0);
            }
            fileSystems.remove(fileSystem.getDevice().getSerialNumber());
            removeItem(fileSystem);

        }
        if (fileSystems.isEmpty()) {
            try {
                removeAllItems();
                insertItemAt("Connected device(s)", 0);
                setSelectedIndex(0);
            } catch (Exception e) {
            }
        }
        revalidate();
        repaint();

    }

    public synchronized void addDevice(IDevice iDevice) {
        Logger.d(this, "addDevice");
        if (iDevice == null) {
            return;
        }

        FileSystem fileSystem = fileSystems.get(iDevice.getSerialNumber());
        if (fileSystem != null) {
            return;
        } else {
            fileSystem = new DeviceFileSystem(iDevice);
            fileSystems.put(iDevice.getSerialNumber(), fileSystem);
            addItem(fileSystem);
        }


        revalidate();
        repaint();


    }

    ListCellRenderer renderer = new ListCellRenderer() {
        @Override
        public Component getListCellRendererComponent(JList jList, Object value, int index, boolean isSelected, boolean cellHasFocus) {
            String text = null;
            if (value == null || value instanceof String) {
                text = (String) value;
            } else if (value instanceof FileSystem) {
                FileSystem fileSystem = (FileSystem) value;
                if (fileSystem.getFSType().equals(FileSystem.DEVICE_FS)) {
                    text = fileSystem.getDevice().getName();

                } else if (fileSystem.getFSType().equals(FileSystem.LOCAL_FS)) {
                    text = "Local FS";
                }
            }


            JLabel label = new JLabel(text);
            return label;
        }


    };

    /*DefaultComboBoxModel model = new DefaultComboBoxModel() {

        private FileSystem selectedFs;
        private final ArrayList<FileSystem> modelfilesystems = new ArrayList<FileSystem>();

        @Override
        public void setSelectedItem(Object o) {
            this.selectedFs = (FileSystem) o;
        }

        @Override
        public Object getSelectedItem() {
            return selectedFs;
        }

        @Override
        public int getSize() {
            return modelfilesystems.size();
        }

        @Override
        public Object getElementAt(int a) {
            return modelfilesystems.get(a);
        }

        @Override
        public void addListDataListener(ListDataListener listDataListener) {

        }

        @Override
        public void removeListDataListener(ListDataListener listDataListener) {

        }
    }; */
}
