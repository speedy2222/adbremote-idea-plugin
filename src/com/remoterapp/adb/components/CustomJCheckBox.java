package com.remoterapp.adb.components;

import javax.swing.*;

/**
 * TODO Add description for class
 *
 * @author Rob Slama
 * @version 1
 */
public class CustomJCheckBox<T> extends JCheckBox{

    private T tag;

    public CustomJCheckBox() {
        super();
    }


    public CustomJCheckBox(String s) {
        super(s);
    }

    public void setTag(T tag){
       this.tag = tag;
    }

    public T getTag(){
        return this.tag;
    }
}
