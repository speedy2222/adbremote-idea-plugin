package com.remoterapp.adb.exception;

/**
 * TODO Add description for class
 *
 * @author Rob Slama
 * @version 1
 */
public class NotSetEnvironment extends Exception{

    public NotSetEnvironment(){
        this("ANDROID_HOME IS NOT SET");
    }

    public NotSetEnvironment(String message){
        super(message);
    }
}
