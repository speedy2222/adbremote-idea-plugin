package com.remoterapp.adb.actions;

import com.android.ddmlib.IDevice;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.remoterapp.adb.filecommander.CommanderMainPanel;


import com.remoterapp.adb.filecommander2.FileCommander;
import com.remoterapp.adb.util.Constants;
import com.remoterapp.adb.util.Logger;

import java.util.TreeMap;

/**
 * TODO Add description for class
 *
 * @author Rob Slama
 * @version 1
 */
public class OpenFileCommander extends AnAction {
    public void actionPerformed(AnActionEvent e) {
        Logger.d(this, "action event");
        FileCommander fileCommander = FileCommander.getInstance();
        if(fileCommander!=null){
            fileCommander.setVisible(true);
        }
        //CommanderMainPanel commanderMainPanel = CommanderMainPanel.getInstance();


        //TreeMap<String, IDevice> map = new TreeMap<String, IDevice>();
        //map.putAll(Constants.CONNECTED_DEVICES);
        //((DeviceCommanderPanel) DeviceCommanderPanel.getInstance(commanderMainPanel)).setDevice(map.firstEntry().getValue());
        //if(commanderMainPanel.getLeftPanel()==null){
        //    commanderMainPanel.setLeftPanel(DeviceCommanderPanel.getInstance(commanderMainPanel));
        //}
        /*try{
            commanderMainPanel.setVisible(true);
        }catch(Throwable th){
            Logger.e(this, th.getMessage());
        } */

        //commanderMainPanel.setRightPanel(LocalFSCommanderPanel.getInstance());
        //commanderMainPanel.setVisible(true);

    }
}
