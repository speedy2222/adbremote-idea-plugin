package com.remoterapp.adb.filecommander;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

/**
 * TODO Add description for class
 *
 * @author Rob Slama
 * @version 1
 */
public class EmptyCommanderPanel extends CommanderPanel{


    public EmptyCommanderPanel(CommanderMainPanel parent, FileSystem fileSystem) {
        super(parent, fileSystem);

    }

    @Override
    public void lostFocusRequest() {

    }

    @Override
    public void setFocus() {

    }


    @Override
    public JPanel getUI() {
        return new JPanel();
    }


}
