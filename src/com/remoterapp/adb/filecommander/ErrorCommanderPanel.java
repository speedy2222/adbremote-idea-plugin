package com.remoterapp.adb.filecommander;

import com.intellij.ui.components.JBScrollPane;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.io.*;

/**
 * TODO Add description for class
 *
 * @author Rob Slama
 * @version 1
 */
public class ErrorCommanderPanel extends CommanderPanel {

    private String text;

    public ErrorCommanderPanel(String errorMessage,CommanderMainPanel parent, FileSystem fileSystem) {
        super(parent,fileSystem);
        this.text = errorMessage;

    }

    @Override
    public void lostFocusRequest() {

    }

    @Override
    public void setFocus() {

    }

    @Override
    public JPanel getUI() {
        JPanel jPanel = new JPanel();
        jPanel.setLayout(new BorderLayout(0, 0));

        JLabel label = new JLabel(text);
        jPanel.add(label, BorderLayout.CENTER);
        return jPanel;
    }



}
