package com.remoterapp.adb.filecommander;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.image.BufferedImage;
import java.io.File;

/**
 * TODO Add description for class
 *
 * @author Rob Slama
 * @version 1
 */
public class ImagePreviewPanel extends JDialog {

    private File imageFile;
    private CommanderPanel parent;

    private ImageIcon imageIcon;
    public ImagePreviewPanel(File imageFile, CommanderPanel parent) {
        this.parent = parent;
        this.imageFile = imageFile;
        setModal(true);
        imageIcon =new ImageIcon(imageFile.getAbsolutePath());
        if(imageIcon.getIconWidth()>500){
            imageIcon = resize(imageIcon);
        }
        JPanel jPanel = getUI();
        add(jPanel);

        setSize(new Dimension(imageIcon.getIconWidth(), imageIcon.getIconHeight()));
        setLocationRelativeTo(null);
        setVisible(true);
        requestFocus();
    }


    public JPanel getUI() {
        JPanel jPanel = new JPanel();
        jPanel.setLayout(new BorderLayout(0, 0));
        JScrollPane jScrollPane = new JScrollPane(new JLabel(imageIcon));
        jPanel.add(jScrollPane);
        jPanel.setVisible(true);

        return jPanel;
    }

    private ImageIcon resize(ImageIcon imageIcon) {
        int origH = imageIcon.getIconHeight();
        int origW = imageIcon.getIconWidth();

        double scale = origW/500;
        int newW = (int) (origW/scale);
        int newH = (int) (origH/scale);

        Image newimg = imageIcon.getImage().getScaledInstance(newW, newH,  java.awt.Image.SCALE_SMOOTH);
        ImageIcon returnIcon = new ImageIcon(newimg);

        return returnIcon;
    }


}
