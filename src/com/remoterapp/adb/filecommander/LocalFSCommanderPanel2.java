package com.remoterapp.adb.filecommander;

import com.android.ddmlib.*;
import com.intellij.icons.AllIcons;
import com.intellij.openapi.progress.ProgressIndicator;
import com.intellij.openapi.progress.ProgressManager;
import com.intellij.openapi.progress.Task;
import com.intellij.ui.components.JBScrollPane;
import com.intellij.ui.table.JBTable;
import com.remoterapp.adb.util.Constants;
import com.remoterapp.adb.util.Logger;
import com.remoterapp.adb.util.Util;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;

/**
 * TODO Add description for class
 *
 * @author Rob Slama
 * @version 1
 */
public class LocalFSCommanderPanel2 extends CommanderPanel {

    private JPanel mainCommanderPanel;
    private static final Object lock = new Object();
    private static CommanderPanel INSTANCE;

    private MyCustomModel myModel;
    private static CommanderMainPanel parent;
    private JBTable jTable;
    private String currentPath;
    protected File currentDirectory;


    private LocalFSCommanderPanel2(CommanderMainPanel parent, FileSystem fileSystem) {
        super(parent, fileSystem);
        this.parent = parent;
        currentPath = System.getProperty("user.home");
        currentDirectory = new File(currentPath);

        initPanel();
    }

    @Override
    public void setFocus() {
        Logger.d(this, "setFocus");
        jTable.requestFocus();
    }


    @Override
    public void lostFocusRequest() {
        Logger.d(this, "lostFocus");
           jTable.transferFocus();
    }

    @Override
    public JPanel getUI() {
        createMenuBar();
        jTable.repaint();
        mainCommanderPanel.revalidate();
        mainCommanderPanel.repaint();
        return mainCommanderPanel;
    }

    public void createMenuBar() {


    }


    public static CommanderPanel getInstance(CommanderMainPanel dialog, FileSystem fileSystem) {
        synchronized (lock) {
            if (INSTANCE == null) {
                INSTANCE = new LocalFSCommanderPanel2(dialog, fileSystem);
            }
            ((LocalFSCommanderPanel2) INSTANCE).parent = dialog;
            return INSTANCE;
        }
    }

    public void refresh() {
        jTable.revalidate();
        jTable.repaint();
    }

    private void initPanel() {
        mainCommanderPanel = new JPanel(new BorderLayout());


        jTable = new JBTable(myModel = new MyCustomModel(this));
        jTable.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        jTable.getColumnModel().getColumn(0).setCellRenderer(new CellNameRenderer());


        JBScrollPane scrollPane = new JBScrollPane(jTable);
        mainCommanderPanel.add(scrollPane, BorderLayout.CENTER);
        jTable.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent mouseEvent) {
                super.mouseClicked(mouseEvent);
                if (mouseEvent.getClickCount() == 2) {
                    int[] rows = jTable.getSelectedRows();
                    if (rows != null && rows.length == 1) {
                        File file = myModel.getValueAtRow(rows[0]);
                        if (file != null && file.isDirectory()) {
                            if (myModel.loadFolder(file)) {
                                jTable.revalidate();
                                jTable.repaint();
                            }
                        }
                    }
                }
            }
        });
        jTable.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent keyEvent) {

            }

            @Override
            public void keyPressed(KeyEvent keyEvent) {
                final int[] rows = jTable.getSelectedRows();

                if (rows != null && rows.length > 0) {
                    if (keyEvent.getKeyCode() == KeyEvent.VK_BACK_SPACE) {
                        if (myModel.loadUpDirectory()) {
                            jTable.revalidate();
                            jTable.repaint();
                        }

                    } else if (keyEvent.getKeyCode() == KeyEvent.VK_ENTER) {

                        if (rows != null && rows.length == 1) {
                            File file = myModel.getValueAtRow(rows[0]);
                            if (file != null && file.isDirectory()) {
                                if (myModel.loadFolder(file)) {
                                    jTable.revalidate();
                                    jTable.repaint();
                                }
                            }
                        }


                    } else if (keyEvent.getKeyCode() == KeyEvent.VK_F5) {
                        copyFiles(rows);

                    } else if (keyEvent.getKeyCode() == KeyEvent.VK_SPACE) {

                    }
                }
            }

            @Override
            public void keyReleased(KeyEvent keyEvent) {

            }


        });
    }


    private void copyFiles(final int[] rows) {
        Task.Modal modal = new Task.Modal(Constants.PROJECT, "copying", true) {

            @Override
            public void onCancel() {
                super.onCancel();
                Logger.d(this, "onCancel");
            }

            @Override
            public void onSuccess() {
                super.onSuccess();
                Logger.d(this, "onSuccess");
            }

            @Nullable
            @Override
            public NotificationInfo notifyFinished() {
                Logger.d(this, "notifyFinished");
                return super.notifyFinished();

            }

            @Override
            public void run(@NotNull ProgressIndicator progressIndicator) {
                final int length = rows.length;
                CommanderPanel rightPanel = parent.getRightPanel();
                if (rightPanel != null && rightPanel instanceof LocalFSCommanderPanel2 && ((LocalFSCommanderPanel2) rightPanel).currentDirectory != null) {

                    double fraction = 1 / (double) length;
                    for (int a = 0; a < length; ++a) {

                        if (progressIndicator.isCanceled()) {
                            return;
                        }
                        File file = myModel.getValueAtRow(rows[a]);

                        if (file.isFile() &&
                                parent.getLeftPanel() != null
                                && parent.getLeftPanel().getFileSystem() != null
                                && parent.getLeftPanel().getFileSystem().getDevice() != null
                                && parent.getLeftPanel().getFileSystem().getDevice().isOnline()) {

                            DeviceCommanderPanel2 commanderPanel2 = (DeviceCommanderPanel2) parent.getLeftPanel();
                            if(commanderPanel2!=null){
                                try {
                                    String localPath = file.getAbsolutePath();
                                    String fileName =  Util.getFileNameFromFile(file, true);
                                    String remotePath = commanderPanel2.myModel.currentRoot.getFullPath()+"/"+fileName;

                                    parent.getLeftPanel().getFileSystem().getDevice().pushFile(localPath, remotePath);

                                    commanderPanel2.refresh();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                } catch (AdbCommandRejectedException e) {
                                    e.printStackTrace();
                                } catch (TimeoutException e) {
                                    e.printStackTrace();
                                } catch (SyncException e) {
                                    e.printStackTrace();
                                }
                            }


                        }


                        ProgressManager.getInstance().getProgressIndicator().setFraction(a * fraction);
                    }
                    ((LocalFSCommanderPanel2) rightPanel).refresh();
                }


            }
        };
        ProgressManager.getInstance().run(modal);

    }


    static class MyCustomModel extends AbstractTableModel {

        private static final String COLUMN_NAME = "name";
        private static final int COLUMN_INDEX_NAME = 0;


        private static final String COLUMN_SIZE = "size";
        private static final int COLUMN_INDEX_SIZE = 1;

        private static final String COLUMN_DATE = "lastMoodified";
        private static final int COLUMN_INDEX_DATE = 2;

        private static final String[] columnsNames = new String[]{COLUMN_NAME, COLUMN_SIZE, COLUMN_DATE};
        private LocalFSCommanderPanel2 parent;

        public MyCustomModel(LocalFSCommanderPanel2 parent) {

            //super(new Object[parent.currentDirectory.listFiles().length][columnsNames.length], columnsNames);
            this.parent = parent;
        }

        @Override
        public int getRowCount() {


            return parent.currentDirectory != null ? parent.currentDirectory.listFiles().length : 0;
        }

        @Override
        public int getColumnCount() {
            return columnsNames.length;
        }

        @Override
        public String getColumnName(int index) {
            return columnsNames[index];
        }

        @Override
        public Class<?> getColumnClass(int index) {
            return getValueAt(0, index).getClass();
        }

        @Override
        public boolean isCellEditable(int row, int col) {
            return false;
        }

        @Override
        public Object getValueAt(int row, int col) {
            switch (col) {
                case COLUMN_INDEX_NAME:
                    return parent.currentDirectory.listFiles()[row];
                case COLUMN_INDEX_SIZE:
                    if (parent.currentDirectory.listFiles()[row].isDirectory()) {
                        return "";
                    } else {
                        return Util.humanReadableByteCount(parent.currentDirectory.listFiles()[row].length(), false);
                    }

                case COLUMN_INDEX_DATE:
                    return sdf.format(parent.currentDirectory.listFiles()[row].lastModified());


            }
            return null;
        }

        public File getValueAtRow(int row) {
            try {
                return parent.currentDirectory.listFiles()[row];
            } catch (Exception e) {
                return null;
            }


        }

        public boolean loadFolder(File fileEntry) {
            if (fileEntry != null && fileEntry.isDirectory()) {
                try {
                    parent.currentDirectory = fileEntry;
                    parent.currentPath = fileEntry.getAbsolutePath();


                    return true;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return false;
        }

        public boolean loadUpDirectory() {
            if (parent.currentDirectory == null || parent.currentDirectory.getParentFile() == null) {
                return false;
            }

            return loadFolder(parent.currentDirectory.getParentFile());
        }

        final SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");

        @Override
        public void setValueAt(Object o, int i, int i2) {

        }

        @Override
        public void addTableModelListener(TableModelListener tableModelListener) {

        }

        @Override
        public void removeTableModelListener(TableModelListener tableModelListener) {

        }
    }

    private class CellNameRenderer extends DefaultTableCellRenderer {
        public CellNameRenderer() {
            setOpaque(true);
        }

        @Override
        public void setValue(Object aValue) {

            if (aValue instanceof File) {
                File entry = (File) aValue;
                if (entry.isDirectory()) {
                    setIcon(AllIcons.Nodes.Folder);
                } else {
                    setIcon(AllIcons.FileTypes.Any_type);
                }
                super.setValue(entry.getName() == null ? "" : entry.getName());
            } else {
                super.setValue(aValue);
            }


        }

        @Override
        public Component getTableCellRendererComponent(JTable jTable, Object o, boolean b, boolean b2, int i, int i2) {
            return super.getTableCellRendererComponent(jTable, o, b, b2, i, i2);
        }
    }
}
