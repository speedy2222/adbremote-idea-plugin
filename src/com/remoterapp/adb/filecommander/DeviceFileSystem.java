package com.remoterapp.adb.filecommander;


import com.android.ddmlib.IDevice;

import java.util.ArrayList;

/**
 * TODO Add description for class
 *
 * @author Rob Slama
 * @version 1
 */
public class DeviceFileSystem extends FileSystem{

    public DeviceFileSystem(IDevice device){
        setDevice(device);
        setFSType(FileSystem.DEVICE_FS);
    }


    @Override
    public FileSystem getFileSystem() {
        return this;
    }
}
