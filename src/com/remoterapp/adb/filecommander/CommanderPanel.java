package com.remoterapp.adb.filecommander;

import com.android.ddmlib.IDevice;
import com.intellij.ui.components.JBScrollPane;
import com.intellij.ui.table.JBTable;
import com.remoterapp.adb.exception.NotSetEnvironment;
import com.remoterapp.adb.util.AdbConnector;

import javax.swing.*;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.ArrayList;

/**
 * TODO Add description for class
 *
 * @author Rob Slama
 * @version 1
 */
public abstract class CommanderPanel{

    private Component parent;

    private FileSystem fileSystem;

    public CommanderPanel(Component parent, FileSystem fileSystem){
        this.parent = parent;
        this.fileSystem = fileSystem;
    }


    public FileSystem getFileSystem(){
        return fileSystem;
    }

    abstract public void lostFocusRequest();
    abstract public void setFocus();

    public JPanel getUI(){
        try {
            throw new Exception("not implemented in concrete class");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


}
