package com.remoterapp.adb.filecommander;

import com.android.ddmlib.*;
import com.intellij.icons.AllIcons;
import com.intellij.openapi.progress.ProgressIndicator;
import com.intellij.openapi.progress.ProgressManager;
import com.intellij.openapi.progress.Task;
import com.intellij.ui.components.JBScrollPane;
import com.intellij.ui.table.JBTable;
import com.remoterapp.adb.util.Constants;
import com.remoterapp.adb.util.Logger;
import com.remoterapp.adb.util.Util;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;

/**
 * TODO Add description for class
 *
 * @author Rob Slama
 * @version 1
 */
public class DeviceCommanderPanel2 extends CommanderPanel {

    public MyModel myModel;
    private CommanderMainPanel parent;
    private JBTable jTable;
    private JPanel mainCommanderPanel;

    public DeviceCommanderPanel2(CommanderMainPanel parent, FileSystem fileSystem) {
        super(parent, fileSystem);
        this.parent = parent;
        initPanel();
    }


    @Override
    public void lostFocusRequest() {

    }

    @Override
    public void setFocus() {

    }

    @Override
    public JPanel getUI() {
        jTable.repaint();
        mainCommanderPanel.revalidate();
        mainCommanderPanel.repaint();
        return mainCommanderPanel;
    }




    private void initPanel() {
        mainCommanderPanel = new JPanel(new BorderLayout());


        jTable = new JBTable(myModel = new MyModel(this));
        jTable.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        jTable.getColumnModel().getColumn(0).setCellRenderer(new CellNameRenderer());


        JBScrollPane scrollPane = new JBScrollPane(jTable);
        mainCommanderPanel.add(scrollPane, BorderLayout.CENTER);
        jTable.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent mouseEvent) {
                super.mouseClicked(mouseEvent);
                if (mouseEvent.getClickCount() == 2) {
                    int[] rows = jTable.getSelectedRows();
                    if (rows != null && rows.length == 1) {
                        FileListingService.FileEntry entry = myModel.getValueAtRow(rows[0]);
                        if (entry != null && entry.isDirectory()) {
                            //go into folder
                            if (myModel.loadFolder(entry)) {
                                jTable.revalidate();
                                jTable.repaint();
                            }
                        }
                    }
                }
            }
        });
        jTable.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent keyEvent) {

            }

            @Override
            public void keyPressed(KeyEvent keyEvent) {
                final int[] rows = jTable.getSelectedRows();

                if (rows != null && rows.length > 0) {
                    if (keyEvent.getKeyCode() == KeyEvent.VK_BACK_SPACE) {
                        if (myModel.loadUpDirectory()) {
                            jTable.revalidate();
                            jTable.repaint();
                        }
                    } else if (keyEvent.getKeyCode() == KeyEvent.VK_ENTER) {
                        FileListingService.FileEntry entry = myModel.getValueAtRow(rows[0]);
                        if (entry != null && entry.isDirectory()) {
                            //go into folder
                            if (myModel.loadFolder(entry)) {
                                jTable.revalidate();
                                jTable.repaint();
                            }
                        }

                    } else if (keyEvent.getKeyCode() == KeyEvent.VK_F5) {

                        copyFiles(rows);

                    } else if (keyEvent.getKeyCode() == KeyEvent.VK_SPACE) {
                        if (rows.length == 1) {
                            FileListingService.FileEntry entry = myModel.getValueAtRow(rows[0]);
                            if (entry != null && entry.getType() == FileListingService.TYPE_FILE) {
                                String extension = Util.getFileExtension(entry.getName());
                                final String hashName = Util.getFileEntryHash(entry);
                                if (Util.isEmpty(extension)) {
                                    extension = "";
                                }
                                final String fileExt = extension;
                                File tempDir = Util.getTemporaryDirectory();
                                if (tempDir != null) {
                                    File[] cachedFile = tempDir.listFiles(new FilenameFilter() {
                                        @Override
                                        public boolean accept(File file, String s) {
                                            return (hashName + "." + fileExt).equals(s);
                                        }
                                    });
                                    if (cachedFile != null && cachedFile.length == 1) {
                                        setPreview(cachedFile[0]);
                                        return;
                                    }
                                }
                                downloadFileForPreview(entry, hashName, extension);
                                return;

                            }
                        }
                    }
                }

            }

            @Override
            public void keyReleased(KeyEvent keyEvent) {

            }
        });
    }


    private void downloadFileForPreview(final FileListingService.FileEntry fileEntry, final String fileHashName, String extension) {
        if (Util.isEmpty(extension)) {
            extension = "";
        }
        final String fileExtension = extension;
        Task.Modal modal = new Task.Modal(Constants.PROJECT, "preparing...", true) {

            @Override
            public void run(@NotNull ProgressIndicator progressIndicator) {
                if (getFileSystem().getDevice() != null && getFileSystem().getDevice().isOnline()) {
                    progressIndicator.setIndeterminate(true);
                    try {
                        getFileSystem().getDevice().pullFile(fileEntry.getFullPath(), Util.getTemporaryDirectory().getAbsolutePath() + "/" + fileHashName + "." + fileExtension);
                        //TODO
                        setPreview(new File(Util.getTemporaryDirectory().getAbsolutePath() + "/" + fileHashName + "." + fileExtension));
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (AdbCommandRejectedException e) {
                        e.printStackTrace();
                    } catch (TimeoutException e) {
                        e.printStackTrace();
                    } catch (SyncException e) {
                        e.printStackTrace();
                        final String errorMessage = e.getMessage();

                    }
                }
            }
        };
        ProgressManager.getInstance().run(modal);
    }

    private void setPreview(final File file) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                String extension = Util.getFileExtension(file.getName());
                if (!Util.isEmpty(extension)) {
                    if ("jpeg".equals(extension.toLowerCase()) ||
                            "jpg".equals(extension.toLowerCase()) ||
                            "png".equals(extension.toLowerCase())) {
                         new ImagePreviewPanel(file, DeviceCommanderPanel2.this);


                    } else if ("xml".equals(extension.toLowerCase()) ||
                            "txt".equals(extension.toLowerCase()) ||
                            "prop".equals(extension.toLowerCase())) {
                        new ImagePreviewPanel(file, DeviceCommanderPanel2.this);

                    }
                } else {
                    new ImagePreviewPanel(file, DeviceCommanderPanel2.this);

                }
            }
        });

    }


    private void copyFiles(final int[] rows) {
        Task.Modal modal = new Task.Modal(Constants.PROJECT, "copying", true) {

            @Override
            public void onCancel() {
                super.onCancel();
                Logger.d(this, "onCancel");
            }

            @Override
            public void onSuccess() {
                super.onSuccess();
                Logger.d(this, "onSuccess");
            }

            @Nullable
            @Override
            public NotificationInfo notifyFinished() {
                Logger.d(this, "notifyFinished");
                return super.notifyFinished();

            }

            @Override
            public void run(@NotNull ProgressIndicator progressIndicator) {
                final int length = rows.length;
                CommanderPanel rightPanel = parent.getRightPanel();
                if(rightPanel !=null && rightPanel instanceof LocalFSCommanderPanel2 && ((LocalFSCommanderPanel2)rightPanel).currentDirectory!=null){

                    double fraction = 1 / (double) length;
                    for (int a = 0; a < length; ++a) {

                        if (progressIndicator.isCanceled()) {
                            return;
                        }
                        FileListingService.FileEntry entry = myModel.getValueAtRow(rows[a]);
                        if (getFileSystem().getDevice() != null && entry != null && entry.getType() == FileListingService.TYPE_FILE) {
                            try {


                                getFileSystem().getDevice().pullFile(entry.getFullPath(), ((LocalFSCommanderPanel2)rightPanel).currentDirectory.getAbsolutePath()+"/" + entry.getName());
                                ProgressManager.getInstance().getProgressIndicator().setText("copy [" + (a + 1) + "/" + length + "] " + entry.getName());
                            } catch (IOException e) {
                                Logger.e(this, e.getMessage());
                            } catch (AdbCommandRejectedException e) {
                                Logger.e(this, e.getMessage());
                            } catch (TimeoutException e) {
                                Logger.e(this, e.getMessage());
                            } catch (SyncException e) {
                                Logger.e(this, e.getMessage());
                            }
                        }
                        ((LocalFSCommanderPanel2) rightPanel).refresh();
                        ProgressManager.getInstance().getProgressIndicator().setFraction(a * fraction);
                    }
                    ((LocalFSCommanderPanel2) rightPanel).refresh();
                }


            }
        };
        ProgressManager.getInstance().run(modal);

    }

    public void refresh() {
        jTable.revalidate();
        jTable.repaint();
    }


    //======================================================================================================================
   //===== table content rendering and handling
   public static class MyModel extends AbstractTableModel {
       private static final String COLUMN_NAME = "name";
       private static final int COLUMN_INDEX_NAME = 0;


       private static final String COLUMN_SIZE = "size";
       private static final int COLUMN_INDEX_SIZE = 1;

       private static final String COLUMN_DATE = "date";
       private static final int COLUMN_INDEX_DATE = 2;

       private static final String COLUMN_TIME = "time";
       private static final int COLUMN_INDEX_TIME = 3;

       private static final String COLUMN_OWNER = "owner";
       private static final int COLUMN_INDEX_OWNER = 4;

       private static final String COLUMN_GROUP = "group";
       private static final int COLUMN_INDEX_GROUP = 5;

       private static final String COLUMN_PERMISSIONS = "permissions";
       private static final int COLUMN_INDEX_PERMISSIONS = 6;

       private static final String COLUMN_INFO = "info";
       private static final int COLUMN_INDEX_INFO = 7;

       private static final String[] columnsNames = new String[]{COLUMN_NAME, COLUMN_SIZE, COLUMN_DATE, COLUMN_TIME, COLUMN_OWNER, COLUMN_GROUP, COLUMN_PERMISSIONS, COLUMN_INFO};
       //private static final String[] columnsNames = new String[]{COLUMN_NAME, COLUMN_SIZE, COLUMN_DATE, COLUMN_TIME, COLUMN_INFO};//, COLUMN_OWNER, COLUMN_GROUP, COLUMN_PERMISSIONS};

       public FileListingService.FileEntry[] currentFolder;
       private FileListingService fileListingService;
       public FileListingService.FileEntry currentRoot;
       private DeviceCommanderPanel2 parent;

       public MyModel(DeviceCommanderPanel2 parent) {
           this.parent = parent;
           if (parent.getFileSystem()!=null && parent.getFileSystem().getDevice() != null) {
               fileListingService = parent.getFileSystem().getDevice().getFileListingService();
               FileListingService.FileEntry root = parent.getFileSystem().getDevice().getFileListingService().getRoot();
               try {
                   currentFolder = fileListingService.getChildrenSync(root);
                   currentRoot = root;
               } catch (TimeoutException e) {
                   e.printStackTrace();
               } catch (AdbCommandRejectedException e) {
                   e.printStackTrace();
               } catch (ShellCommandUnresponsiveException e) {
                   e.printStackTrace();
               } catch (IOException e) {
                   e.printStackTrace();
               }
           }

       }

       public void updateDevice() {
           if (parent.getFileSystem().getDevice() != null) {
               fileListingService = parent.getFileSystem().getDevice().getFileListingService();
               FileListingService.FileEntry root = parent.getFileSystem().getDevice().getFileListingService().getRoot();
               try {
                   currentRoot = root;
                   currentFolder = fileListingService.getChildrenSync(root);
               } catch (TimeoutException e) {
                   e.printStackTrace();
               } catch (AdbCommandRejectedException e) {
                   e.printStackTrace();
               } catch (ShellCommandUnresponsiveException e) {
                   e.printStackTrace();
               } catch (IOException e) {
                   e.printStackTrace();
               }
               //fireTableDataChanged();
           }

       }

       @Override
       public int getRowCount() {
           return currentFolder == null ? 0 : currentFolder.length;
       }

       @Override
       public int getColumnCount() {
           return columnsNames.length;
       }

       @Override
       public String getColumnName(int columnIndex) {
           return columnsNames[columnIndex];
       }

       @Override
       public Class<?> getColumnClass(int columnIndex) {
           return getValueAt(0, columnIndex).getClass();
       }

       @Override
       public boolean isCellEditable(int rowIndex, int columnIndex) {
           return false;
       }

       @Override
       public Object getValueAt(int rowIndex, int columnIndex) {
           FileListingService.FileEntry fileEntry = currentFolder[rowIndex];
           if (fileEntry != null) {
               switch (columnIndex) {
                   case COLUMN_INDEX_NAME:
                       return fileEntry;
                   case COLUMN_INDEX_SIZE:
                       if (fileEntry.isDirectory()) {
                           return "";
                       } else {
                           return fileEntry.getSize() == null ? "0" : Util.humanReadableByteCount(fileEntry.getSize(), false);
                       }

                   case COLUMN_INDEX_DATE:
                       return fileEntry.getDate() == null ? "" : fileEntry.getDate();
                   case COLUMN_INDEX_TIME:
                       return fileEntry.getTime() == null ? "" : fileEntry.getTime();
                   case COLUMN_INDEX_OWNER:
                       return fileEntry.getOwner() == null ? "" : fileEntry.getOwner();
                   case COLUMN_INDEX_GROUP:
                       return fileEntry.getGroup() == null ? "" : fileEntry.getGroup();
                   case COLUMN_INDEX_PERMISSIONS:
                       return fileEntry.getPermissions() == null ? "" : fileEntry.getPermissions();
                   case COLUMN_INDEX_INFO:
                       return fileEntry.getInfo() == null ? "" : fileEntry.getInfo();

               }
           }
           return null;
       }

       @Override
       public void setValueAt(Object value, int rowIndex, int columnIndex) {

       }

       @Override
       public void addTableModelListener(TableModelListener tableModelListener) {

       }

       @Override
       public void removeTableModelListener(TableModelListener tableModelListener) {

       }

       public FileListingService.FileEntry getValueAtRow(int row) {
           try {
               return currentFolder[row];
           } catch (Exception e) {
               return null;
           }


       }

       public boolean loadFolder(FileListingService.FileEntry fileEntry) {
           if (fileEntry != null && fileEntry.isDirectory()) {
               try {
                   currentFolder = fileListingService.getChildrenSync(fileEntry);
                   currentRoot = fileEntry;
                   //fireTableDataChanged();
                   return true;
               } catch (TimeoutException e) {
                   e.printStackTrace();
               } catch (AdbCommandRejectedException e) {
                   e.printStackTrace();
               } catch (ShellCommandUnresponsiveException e) {
                   e.printStackTrace();
               } catch (IOException e) {
                   e.printStackTrace();
               }
           }
           return false;
       }

       public boolean loadUpDirectory() {
           if (currentRoot == null || currentRoot.isRoot()) {
               return false;
           }

           return loadFolder(currentRoot.getParent());
       }
   }

    private class CellNameRenderer extends DefaultTableCellRenderer {
        public CellNameRenderer() {
            setOpaque(true);
        }

        @Override
        public void setValue(Object aValue) {

            if (aValue instanceof FileListingService.FileEntry) {
                FileListingService.FileEntry entry = (FileListingService.FileEntry) aValue;
                if (entry.isDirectory()) {
                    setIcon(AllIcons.Nodes.Folder);
                } else {
                    setIcon(AllIcons.FileTypes.Any_type);
                }
                super.setValue(entry.getName() == null ? "" : entry.getName());
            } else {
                super.setValue(aValue);
            }


        }

        @Override
        public Component getTableCellRendererComponent(JTable jTable, Object o, boolean b, boolean b2, int i, int i2) {
            return super.getTableCellRendererComponent(jTable, o, b, b2, i, i2);
        }
    }
}
