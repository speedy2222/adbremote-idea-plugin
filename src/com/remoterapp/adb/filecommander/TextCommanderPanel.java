package com.remoterapp.adb.filecommander;

import com.intellij.ui.components.JBScrollPane;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.io.*;

/**
 * TODO Add description for class
 *
 * @author Rob Slama
 * @version 1
 */
public class TextCommanderPanel extends CommanderPanel {

    private String text;

    public TextCommanderPanel(File textFile, CommanderMainPanel parent) {
        super(parent, null);
        if (textFile != null) {
            BufferedReader br = null;
            try {
                br = new BufferedReader(new FileReader(textFile));
                StringBuilder sb = new StringBuilder();
                String line = br.readLine();

                while (line != null) {
                    sb.append(line);
                    sb.append(System.getProperty("line.separator"));
                    line = br.readLine();
                }
                text = sb.toString();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    @Override
    public void lostFocusRequest() {

    }

    @Override
    public void setFocus() {

    }

    @Override
    public JPanel getUI() {
        JPanel jPanel = new JPanel();
        jPanel.setLayout(new BorderLayout(0, 0));

        JEditorPane editorPane = new JEditorPane();
        int caretPosition = editorPane.getCaretPosition();
        editorPane.setText(text);
        editorPane.setCaretPosition(Math.min(caretPosition, text.length()));

        JBScrollPane scrollPane = new JBScrollPane(editorPane);

        jPanel.add(scrollPane, BorderLayout.CENTER);
        return jPanel;
    }



}
