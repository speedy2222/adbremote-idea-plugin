package com.remoterapp.adb.filecommander;

import com.android.ddmlib.IDevice;

/**
 * TODO Add description for class
 *
 * @author Rob Slama
 * @version 1
 */
public abstract class FileSystem {

    public static final String LOCAL_FS = "local_fs";
    public static final String DEVICE_FS = "device_fs";

    private IDevice DEVICE;
    private String fsType;

    public FileSystem getFileSystem(){
        return null;
    }


    public IDevice getDevice(){
        return this.DEVICE;
    }

    public void setDevice(IDevice DEVICE){
        this.DEVICE = DEVICE;
    }

    public void setFSType(String fsType){
        this.fsType = fsType;
    }

    public String getFSType(){
        return this.fsType;
    }
}
