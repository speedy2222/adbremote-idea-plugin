package com.remoterapp.adb.filecommander;

import com.android.ddmlib.IDevice;
import com.intellij.icons.AllIcons;
import com.intellij.openapi.ui.ComboBox;
import com.intellij.openapi.util.IconLoader;
import com.remoterapp.adb.exception.NotSetEnvironment;
import com.remoterapp.adb.util.AdbConnector;
import com.remoterapp.adb.util.Util;

import javax.swing.*;
import javax.swing.event.ListDataListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * TODO Add description for class
 *
 * @author Rob Slama
 * @version 1
 */
public class CommanderMainPanel extends JDialog {

    private static CommanderMainPanel INSTANCE;
    private static final Object lock = new Object();
    private CommanderPanel leftPanel, rightPanel;
    private JSplitPane mainPanelUI;
    private final ArrayList<FileSystem> fileSystems = new ArrayList<FileSystem>();

    private final JPanel leftBasePanel = new JPanel(new BorderLayout());

    private final HashMap<String, DeviceCommanderPanel2> leftPanelsMap = new HashMap<String, DeviceCommanderPanel2>();

    private CommanderMainPanel() {
        setMinimumSize(new Dimension(600, 400));
        setModal(true);
        setLocationRelativeTo(null);
        init();
        setLayout(new BorderLayout());
        mainPanelUI.setAlignmentY(Component.TOP_ALIGNMENT);
        add(mainPanelUI, BorderLayout.CENTER);
        ArrayList<FileSystem> fs = Util.getAvalFS();
        if (fs != null || !fs.isEmpty()) {
            fileSystems.addAll(fs);
        }
        if (fileSystems.size() > 0) {
            String serialNo = fileSystems.get(0).getDevice().getSerialNumber();
            leftPanelsMap.put(serialNo,new DeviceCommanderPanel2(this, fileSystems.get(0)));
            setLeftPanel(leftPanelsMap.get(fileSystems.get(0).getDevice().getSerialNumber()));
        }
        setRightPanel(LocalFSCommanderPanel2.getInstance(this, null));


    }

    private void init() {
        mainPanelUI = new JSplitPane();
        mainPanelUI.setResizeWeight(.5);
        updateUI();
    }

    public static CommanderMainPanel getInstance() {
        synchronized (lock) {
            if (INSTANCE == null) {
                INSTANCE = new CommanderMainPanel();
            }
            return INSTANCE;
        }
    }

    public void menuBar() {
        JMenuBar jMenuBar = new JMenuBar();

        JMenuItem jMenuItem = new JMenuItem("", AllIcons.General.AddJdk);
        jMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                switchPanels();
            }
        });


        jMenuBar.add(jMenuItem);
        setJMenuBar(jMenuBar);
    }

    public CommanderPanel getLeftPanel() {
        return leftPanel;
    }

    public CommanderPanel getRightPanel() {
        return rightPanel;
    }

    public void setLeftPanel(CommanderPanel leftPanel) {
        this.leftPanel = leftPanel;
        this.updateUI();
    }

    public void setRightPanel(CommanderPanel rightPanel) {
        this.rightPanel = rightPanel;
        this.updateUI();
    }

    public void switchPanels() {
        CommanderPanel temp = leftPanel;
        leftPanel = rightPanel;
        rightPanel = temp;
        this.updateUI();
    }

    private void updateUI() {
        if (leftPanel != null) {

            leftBasePanel.add(leftPanel.getUI(), BorderLayout.CENTER);
        } else {
            leftBasePanel.add(new EmptyCommanderPanel(this, null).getUI(), BorderLayout.CENTER);

        }
        mainPanelUI.setLeftComponent(leftBasePanel);

        if (rightPanel != null) {
            mainPanelUI.setRightComponent(rightPanel.getUI());
        } else {
            mainPanelUI.setRightComponent(new EmptyCommanderPanel(this, null).getUI());
        }
        mainPanelUI.revalidate();
        mainPanelUI.repaint();
    }


}
