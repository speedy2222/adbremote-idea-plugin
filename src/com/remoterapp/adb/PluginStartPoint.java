package com.remoterapp.adb;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.wm.ToolWindow;
import com.intellij.openapi.wm.ToolWindowFactory;
import com.intellij.ui.content.Content;
import com.intellij.ui.content.ContentFactory;
import com.remoterapp.adb.exception.NotSetEnvironment;
import com.remoterapp.adb.ui.ADBNotConfiguredPanel;
import com.remoterapp.adb.ui.MainUI;
import com.remoterapp.adb.util.AdbConnector;
import com.remoterapp.adb.util.Constants;
import com.remoterapp.adb.util.Logger;

import java.util.HashSet;

/**
 * TODO Add description for class
 *
 * @author Rob Slama
 * @version 1
 */
public class PluginStartPoint implements ToolWindowFactory {


    protected ToolWindow toolWindow;
    protected ContentFactory contentFactory;
    protected MainUI mainUI;
    private static final HashSet<PluginStartPoint> instances = new HashSet<PluginStartPoint>();

    public PluginStartPoint() {
        instances.add(this);
    }

    @Override
    public void createToolWindowContent(Project project, ToolWindow toolWindow) {
        Constants.PROJECT = project;
        this.toolWindow = toolWindow;
        this.contentFactory = ContentFactory.SERVICE.getInstance();

        init();
    }


    private void init() {
        try {
            long time;
            Logger.d(this.getClass(), time = System.currentTimeMillis());
            AdbConnector.getInstance();
            long _time = System.currentTimeMillis();
            Logger.d(this.getClass(), _time - time);
            time = _time;
            Content content = contentFactory.createContent(mainUI = new MainUI(), "", true);

            this.toolWindow.getContentManager().addContent(content);
            _time = System.currentTimeMillis();
            Logger.d(this.getClass(), _time - time);
        } catch (NotSetEnvironment notSetEnvironment) {
            //notSetEnvironment.printStackTrace();
            Content content = contentFactory.createContent(new ADBNotConfiguredPanel(this), "", true);
            this.toolWindow.getContentManager().addContent(content);
        }
    }

    public static void reloadPluginUI() {
        if (!instances.isEmpty()) {
            for(PluginStartPoint instance: instances){
                instance.toolWindow.getContentManager().removeAllContents(true);
                MainUI.getInstances().remove(instance.mainUI);
                instance.mainUI = new MainUI();
                instance.toolWindow.getContentManager().addContent(instance.contentFactory.createContent(instance.mainUI,"",true));
            }

        }
    }

}
